;;; bitbake.el --- Major modes for editing bitbake bb* and conf files.

;; Copyright (C) 2012-2024 Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Maintainer: Ola Nilsson <ola.nilsson@gmail.com>
;; Created: 4 Dec 2012
;; Keywords: conf bitbake languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; The `bitbake-mode' and `conf-bitbake-mode' modes are designed to
;; edit the various configuration, recipe, and class files used by
;; Bitbake.

;; `bitbake-mode' is meant for .bb, .bbclass, and .bbappend files,
;; while `conf-bitbake-mode' is meant for .conf files.  Strictly
;; speaking `bitbake-mode' can also be used for .conf files, but
;; `conf-bitbake-mode' is more lightweight as it does not use
;; `mmm-mode'.

;;; Code:

;; Declarations
(declare-function mmm-regions-in "mmm" (start stop))

;; Too many files have to be loaded always.  It would be nice to
;; autoload them instead
(require 'bitbake-compat)
(require 'bitbake-functions)
(require 'bitbake-mmm)
(require 'bitbake-ff)
(require 'conf-bitbake-mode)

;; Customization:

(defgroup bitbake nil
  "Customization options for bitbake-mode et al."
  :group 'tools)

(defface bitbake-python-function-face
  '((((type graphic) (background light)) :background "azure1" :extend t)
	(((type tty) (class color) (background light)) :background "cyan" :extend t)
	(((type tty)) :background "cyan" :extend t)
	)
  "Face for python functions in bitbake files."
  :group 'bitbake)

(defface bitbake-python-task-face
  '((((type graphic) (background light)) :background "honeydew2" :extend t)
	(((type tty) (class color) (background light)) :background "color-254" :extend t)
	(((type tty)) :background "blue" :extend t))
  "Face for python methods in bitbake files."
  :group 'bitbake)

(defface bitbake-shell-function-face
  '((((type graphic) (background light)) :background "LavenderBlush" :extend t)
	(((type tty) (class color) (background light)) :background "color-224" :extend t)
    (((type tty)) :background "brightblack" :extend t))
  "Face for shell functions in bitbake files."
  :group 'bitbake)

(defface bitbake-python-expansion-face
  '((default :background "ivory2"))
  "Face for python code embedded in bitbake variables."
  :group 'bitbake)

;; Common parts:

(defvar bitbake-mode-syntax-table
  (let ((st (make-syntax-table)))
	;; Comments start with # and end at eol
	(modify-syntax-entry ?#	  "<" st)
	(modify-syntax-entry ?\n  ">" st)
	(modify-syntax-entry ?\^m ">" st)
	(modify-syntax-entry ?\"  "\""  st) ;strings are delimited by "
	(modify-syntax-entry ?\'  "\""  st) ;strings are delimited by '
	(modify-syntax-entry ?\\  "\\"  st) ;backslash is escape
	;; (modify-syntax-entry ?!   "'"   st) ;! is 'expression quote or prefix operator'
	;; (modify-syntax-entry ?_   "w"   st) ;_ has word syntax
	st)
  "Syntax table for `bitbake-mode'.")

(defun bitbake-comment-setup ()
  "Setup local comment variables for `bitbake-mode'."
  (set (make-local-variable 'comment-start) "#")
  (set (make-local-variable 'comment-start-skip) "#+[ \t]*")
  (set (make-local-variable 'comment-indent-function) 'comment-indent-default)
  (set (make-local-variable 'comment-style) 'plain)
  (set (make-local-variable 'comment-continue) nil))

;; regexps from BBHander - parser for bb file
;;__func_start_regexp__    = re.compile( r"(((?P<py>python)|(?P<fr>fakeroot))\s*)*(?P<func>[\w\.\-\+\{\}\$]+)?\s*\(\s*\)\s*{$" )
;;__inherit_regexp__       = re.compile( r"inherit\s+(.+)" )
;;__export_func_regexp__   = re.compile( r"EXPORT_FUNCTIONS\s+(.+)" )
;;__addtask_regexp__       = re.compile("addtask\s+(?P<func>\w+)\s*((before\s*(?P<before>((.*(?=after))|(.*))))|(after\s*(?P<after>((.*(?=before))|(.*)))))*")
;;__addhandler_regexp__    = re.compile( r"addhandler\s+(.+)" )
;;__def_regexp__           = re.compile( r"def\s+(\w+).*:" )
;;__python_func_regexp__   = re.compile( r"(\s+.*)|(^$)" )

(defvar conf-bitbake-font-lock-keywords)

(defun bitbake-mode-font-lock-keywords ()
  "Return the default font lock keywords for `bitbake-mode'."
  ;;TODO: create specific faces
  (append
   conf-bitbake-font-lock-keywords
   `("inherit\\(?:_defer\\)?"
	 "before"
	 "after"
	 "EXPORT_FUNCTIONS"
	 "addtask"
	 "deltask"
	 "addhandler"
     ("\\<inherit\\(?:_defer\\)?\\>" ("[^ \t\n]+" nil nil (0 font-lock-type-face)))
	 ("EXPORT_FUNCTIONS[ \t]+\\([^ \t\n]+\\)" 1 font-lock-function-name-face)
	 ("addtask[ \t]+\\([^ \t\n]+\\)" 1 font-lock-variable-name-face)
	 ("deltask[ \t]+\\([^ \t\n]+\\)" 1 font-lock-variable-name-face)
	 ("\\<\\(?:after\\|before\\)\\>[ \t]+\\([^ \t\n]+\\)" 1 font-lock-variable-name-face)
	 ("\\<addhandler\\>[ \t]+\\(.*\\)" 1 font-lock-variable-name-face)
     ("\\(?:^\\|\\s-\\)\\(python\\)\\>" 1 'font-lock-keyword-face)
     ("\\(?:^\\|\\s-\\)\\(fakeroot\\)\\>" 1 'font-lock-keyword-face)
     ("^\\(?:fakeroot\\s-+\\)?\\(?:python\\)\\s-+\\(?:fakeroot\\s-+\\)?\\(?1:\\(?:\\sw\\|[:_-]\\)+\\)[ \t]*("
      1 'font-lock-function-name-face)
;	 ("\\${[[:alpha]]+}" . 'font-lock-variable-name-face )
	 )
   ))

(defcustom bitbake-shell-extra-keywords
  '("bbnote" "bbwarn" "bberror" "bbfatal_log" "bbfatal"
    ;; Allow for : (override character) in function names
    ("^\\(\\(?:\\sw\\|:\\)+\\)[ \t]*(" 1 font-lock-function-name-face))
  "Extra font-lock keyword matchers for bitbake embedded shells.
Each item can be either a regexp or a matcher function as
described in Info node `(elisp) Search-based Fontification'."
  :group 'bitbake
  :type '(repeat (choice (regexp :tag "Regexp")
						 (function :tag "Matcher function")))
  :link '(info-link "(elisp) Search-based Fontification")
  )

(defun bb-sh-mode ()
  "Force shell type in `shell-script-mode' to be `sh'."
  (let ((sh-shell-file "/bin/sh"))
	(shell-script-mode)
	(font-lock-add-keywords nil bitbake-shell-extra-keywords)))

(defcustom bitbake-topdir-dominating-file '("LICENSE")
  "Files used to identify `bitbake-topdir'.
When `bitbake-mode' intializes its local variables it uses
`locate-dominating-file' to search for the files until one is
found.  `bitbake-topdir' is set to the directory of the found
file.  If no file is found `bitbake-topdir is left at its default
value."
  :group 'bitbake
  :type '(repeat string)
  :link '(variable-link bitbake-topdir))

(defcustom bitbake-topdir ""
  "The top dir of this bitbake/poky tree.
Set by `bitbake-mode' to a buffer local value, see `bitbake-topdir-dominating-file'."
  :group 'bitbake
  :type 'string
  :link '(variable-link bitbake-topdir-dominating-file))
(make-variable-buffer-local 'bitbake-topdir)

(defun bitbake-imenu-create-index ()
  "Create an imenu table of section headers.
Adds defs, python functions, shell functions, and
EXPORT_FUNCTIONS to the imenu."
  (let (py-def-menu sh-menu other-menu function-name
		(py-def-re "^\\(?:def\\|\\(?:fakeroot[ \t]+\\)?python\\)\\(?:[ \t]+\\([^ \t]+\\)\\)?[ \t]*(")
		(sh-re "^\\(?:fakeroot[ \t]+\\)?\\([[:alnum:]_]+\\)[[:blank:]]*()")
		(exp-fn-re "^[ \t]*\\(EXPORT_FUNCTIONS\\)")
		(addtask-re "^[ \t]*addtask[ \t]+\\([a-zA-Z0-9]+\\)"))

    (save-excursion
      (save-match-data
        (save-restriction
          (widen)
          ;;build the list backwards so it doesn't have to be reversed later
          (goto-char (1value (point-min)))
          (while (re-search-forward py-def-re nil t)
			(when (null (setq function-name (match-string-no-properties 1)))
			  (setq function-name "ANONYMOUS"))
            (add-to-list 'py-def-menu (cons function-name (match-beginning 0))))
          (goto-char (1value (point-min)))
          (while (re-search-forward sh-re nil t)
			(when (not (string= "python" (setq function-name (match-string-no-properties 1))))
			  (add-to-list 'sh-menu (cons function-name (match-beginning 0)))))
		  (goto-char (1value (point-min)))
		  (when (re-search-forward "^[ \t]*\\(EXPORT_FUNCTIONS\\)" nil t)
			(setq function-name (match-string-no-properties 1))
			(add-to-list 'other-menu (cons function-name (match-beginning 0))))
		  )))
    (append py-def-menu sh-menu other-menu)))

(defvar imenu-max-items)

(declare-function imenu--sort-by-name "imenu")

;;;###autoload
(define-derived-mode bitbake-mode prog-mode "BitBake"
  "Major mode for editing BitBake files."
  (when (buffer-file-name) ;;mmm-mode does something funny here
	(set (make-local-variable 'bitbake-topdir)
		 (locate-dominating-file (buffer-file-name) "LICENSE")))
  (bitbake-comment-setup)
  (setq-local comment-auto-fill-only-comments t)
  (auto-fill-mode)
  (set (make-local-variable 'font-lock-defaults)
	   '(bitbake-mode-font-lock-keywords))
  ;;(set (make-local-variable 'indent-line-function) 'bitbake-indent-line)
  (setq-local electric-indent-inhibit t)
  (bitbake-ff-setup)
  (when (require 'imenu "imenu" t)
	(setq-local imenu-max-items 30)
	(setq-local imenu-sort-function #'imenu--sort-by-name)
	(setq-local imenu-create-index-function #'bitbake-imenu-create-index))
  (setq-local beginning-of-defun-function #'bitbake-nav-beginning-of-defun)
  (bitbake-mmm-setup))

;;;
;;;
;;; Section: bitbake file navigation functions

(defvar bitbake--nav-beginning-of-defun-regex
  "^[ \t]*\\(?:\\(?:\\(?:python\\|fakeroot\\)[ \t]+\\)*[a-zA-Z_]+[ \t]*([ \t]*)\\|def[ \t]*\\)"
  "Regexp matching function definition.")

(defun bitbake-nav-beginning-of-defun (&optional arg)
  "Move point to `beginning-of-defun'.
With positive ARG search backwards else search forward.  When ARG
is nil or 0 defaults to 1.  When searching backwards nested
defuns are handled with care depending on current point position.
Return non-nil if point is moved to `beginning-of-defun'."
  (when (or (null arg) (= arg 0)) (setq arg 1))
  (let (found
		(re-fn (if (> arg 0) #'re-search-backward #'re-search-forward)))
	(cond ((and (eq this-command 'mark-defun)
				(looking-at-p bitbake--nav-beginning-of-defun-regex)))
		  (t
		   (when (and (< arg 0)
					  (looking-at-p bitbake--nav-beginning-of-defun-regex))
              (end-of-line 1))
			(dotimes (i (if (> arg 0) arg (- arg)))
			  (when (funcall re-fn bitbake--nav-beginning-of-defun-regex nil t)
				(setq found t)))))
	found))


;;; add keys to bitbake-mode-map here
;;(define-key bitbake-mode-map key binding)

;;;###autoload
(defalias 'bb-mode 'bitbake-mode)

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.bb\\(class\\|append\\)?\\'" . bitbake-mode))

;;;
;;;
;;; Section: auto-insert

(defun bitbake-auto-insert-header ()
  "Insert copyright header for bitbake files."
  (insert "# Copyright (C) " (substring (current-time-string) -4)
		  " " user-full-name " <" user-mail-address ">" "\n"
		  "# Released under the MIT License (see COPYING.MIT for the terms)\n"))

(defun bitbake-auto-insert ()
  "Insert a Bitbake skeleton as in bitbake/contrib/newbb.vim.
Intended to be used with `auto-insert' et al."
  (bitbake-auto-insert-header)
  (insert "DESCRIPTION = \"\"\n"
		  "HOMEPAGE = \"\"\n"
		  "LICENSE = \"\"\n"
		  "SECTION = \"\"\n"
		  "DEPENDS = \"\"\n"
		  "\n"
		  "SRC_URI = \"\"\n")
  (goto-char (point-min))
  (search-forward "DESCRIPTION = \""))

(define-auto-insert (cons "\\.bb\\'" "Bitbake skeleton") 'bitbake-auto-insert)
(add-hook 'bitbake-mode-hook 'auto-insert)
;;;
;;;
;;; Section: useful bitbake stuff

(defun bitbake-ws-around-assignments-cleanup ()
  "Make sure there is a single space surrounding assignment ops."
  (interactive "*")
  (let ((re (concat "^[[:blank:]]*"                     ;start of line and leading blanks
					"[^[#\n[:blank:]]+[^.+:?[:blank:]]" ;variable name, not a comment
					"\\(?:\\[.+\\]\\)?"                 ;optional variable flag
					"\\([[:blank:]]*"                   ;blank before assignment op
					"\\([:+.]=\\|=[.+]?\\|\\?\\??=\\)"  ;assignment op
					"[[:blank:]]*\\)")))                ;blank after assignment op
	(save-excursion
	  (save-match-data
		(save-restriction
		  (widen)
		  (goto-char (point-min))
		  (while (re-search-forward re nil t)
			(when (and
				   ;; do not replace with the same string - modifies buffer for no reason
				   (not (string= (format " %s " (match-string 2)) (match-string 1)))
				   ;; do not replace unless for actual bitbake variables
				   (null (remove 'bitbake-mode (mapcar 'car (mmm-regions-in (match-beginning 0)
																			(match-end 0))))))
			  (replace-match " \\2 " nil nil nil 1))))))))

(defun bitbake-inc-pr ()
  "Attempt to increment the last number of any PR variable."
  (interactive)
  (save-excursion
	(goto-char (point-max))
	(when (re-search-backward "^[[:blank:]]*PR[[:blank:]]*[+=:]?=[[:blank:]]*\".*\\([0-9]+\\)\"" nil t)
	  (replace-match (number-to-string (1+ (string-to-number (match-string 1)))) t t nil 1))))

(defvar bb-dired-numfile-alist)
(defun bitbake-dired-collect-numfiles (fn)
  "Add numfiles information to `bb-dired-numfile-alist' for file FN.
If FN ends with `.[0-9]+' add an element of the form (FILENAME
. NUMBER-LIST ) to `bb-dired-number-alist'.  FILENAME is FN sans
the number suffix.  NUMBER-LIST is a list of strings where each
element is a number suffix for an existing file with the same
FILENAME base.  `bb-dired-numfile-alist' is normally locally
bound by the calling function."
  (when (string-match "temp/\\(run\\|log\\)\\..*\\.[0-9]+$" fn)
	(let ((fn (replace-regexp-in-string "\\.[0-9]+$" "" fn)))
	  (unless (assoc fn bb-dired-numfile-alist)
		(let* ((base-numfiles (concat (file-name-nondirectory fn) "."))
			   (possibilities (file-name-all-completions base-numfiles
														 (file-name-directory fn)))
			   (numbers (mapcar (lambda (numfile)
								  (string-match "\\.\\([0-9]+\\)$" numfile)
								  (match-string 1 numfile)) possibilities)))
		  (when numbers
			(setq bb-dired-numfile-alist (cons (cons fn numbers)
											   bb-dired-numfile-alist))))))))


(defvar dired-del-marker)
(defvar dired-mode-map)
(defun bitbake-dired-trample-numfiles (fn)
  "Mark FN to be deleted if doen't match `bb-dired-numfile-alist'.
If FN ends with `.[0-9]+' and the number suffix is not in
`bb-dired-numfile-alist'.  `bb-dired-numfile-alist' is normally
locally bound and has elements of type (BASENAME . NUMBER-LIST)
where NUMBER-LIST is a list of string."
  (let* ((start-vn (string-match "\\.[0-9]+$" fn))
		 base-version-list)
    (and start-vn
		 (setq base-version-list	; there was a base version to which
			   (assoc (substring fn 0 start-vn)	; this looks like a
					  bb-dired-numfile-alist))	; subversion
		 (not (member (substring fn (+ 1 start-vn))
					  base-version-list))	; this one doesn't make the cut
		 (progn (beginning-of-line)
				(delete-char 1)
				(insert dired-del-marker)))))

(declare-function dired-map-dired-file-lines "dired-aux" (fun))
(declare-function dired-clean-directory "dired-aux" (keep))
;;;###autoload
(defun bitbake-dired-clean-logfiles (keep)
  "Flag numerical backups for deletion.
Spares `dired-kept-versions' latest versions, and `kept-old-versions' oldest.
Positive prefix arg KEEP overrides `dired-kept-versions';
Negative prefix arg KEEP overrides `kept-old-versions' with KEEP made positive.

To clear the flags on these files, you can't use \\[dired-flag-backup-files]
with a prefix argument.  Some other function must be implemented.
Based on `dired-clean-directory'."
  (interactive "P")
  (setq keep (if keep (prefix-numeric-value keep) dired-kept-versions))
  (let ((early-retention (if (< keep 0) (- keep) kept-old-versions))
		(late-retention (if (<= keep 0) dired-kept-versions keep))
		(bb-dired-numfile-alist ()))
    (message "Cleaning bitbake log and run files (keeping %d late, %d old)..."
			 late-retention early-retention)
    ;; Look at each file.
    ;; If the file has numeric backup versions,
    ;; put on bb-dired-file-version-alist an element of the form
    ;; (FILENAME . VERSION-NUMBER-LIST)
    (dired-map-dired-file-lines (function bitbake-dired-collect-numfiles))
    ;; Sort each VERSION-NUMBER-LIST,
    ;; and remove the versions not to be deleted.
    (let ((fval bb-dired-numfile-alist))
      (while fval
		(let* ((sorted-v-list (cons 'q (sort (cdr (car fval)) (lambda (num1 num2)
																(let ((f1 (concat (caar fval) "." num1))
																	  (f2 (concat (caar fval) "." num2)))
																  (file-newer-than-file-p f1 f2))
																))))
			   (v-count (length sorted-v-list)))
		  (if (> v-count (+ early-retention late-retention))
			  (rplacd (nthcdr early-retention sorted-v-list)
					  (nthcdr (- v-count late-retention)
							  sorted-v-list)))
		  (rplacd (car fval)
				  (cdr sorted-v-list)))
		(setq fval (cdr fval))))
    ;; Look at each file.  If it is a numeric backup file,
    ;; find it in a VERSION-NUMBER-LIST and maybe flag it for deletion.
    (dired-map-dired-file-lines (function bitbake-dired-trample-numfiles))
    (message "Cleaning bitbake log and run files ...done")))

(defun bitbake-dired-clean-directory (keep)
  "Flag bitbake logfiles and Emacs numerical backups for deletion.
KEEP is passed to `bitbake-dired-clean-logfiles' and `dired-clean-directory'."
  (interactive "P")
  (bitbake-dired-clean-logfiles keep)
  (dired-clean-directory keep))

;;###autoload
(add-hook 'dired-mode-hook (lambda () (define-key dired-mode-map (kbd ".") 'bitbake-dired-clean-directory)))

;;; Section: bitbake log files

(defgroup bitbake-log nil
  "Customization options for bitbake-task-log-mode."
  :group 'bitbake)

(defcustom bitbake-task-log-debug-face 'font-lock-comment-face "." :type 'face :group 'bitbake-log)
(defcustom bitbake-task-log-note-face 'font-lock-constant-face "." :type 'face :group 'bitbake-log)

;;TODO: not used and not tested, should replace the special construct with empty regex
(defcustom bitbake-log-other-file-alist
  '(("/run\\.[^/]+\\'"  bitbake-task-log-runfile)
	)
  "Alist of extensions to find given the current file's extension.

This list should contain the most used extensions before the others,
since the search algorithm searches sequentially through each directory
specified in `ff-search-directories'.  If a file is not found, a new one
is created with the first matching extension."
  :type '(repeat (list regexp (choice (repeat string) function)))
  :group 'bitbake-log)


;; (defvar bitbake-task-log-mode-syntax-table
;;   (let ((table (make-
(defvar bitbake-task-log-mode-font-lock-keywords
  '(("^DEBUG:" . font-lock-comment-face)
	("^NOTE:" . font-lock-constant-face)
	("^WARNING:" . font-lock-warning-face)
    ("Executing.* function \\([^[:space:]]+\\)" 1 font-lock-function-name-face)
	("\\<[[:upper:]][[:upper:][:digit:]_]+\\>" . font-lock-variable-name-face)
	("function \\(.*\\) finished" 1 font-lock-function-name-face)
	)
  "Keywords to highlight in BitBake task log mode."
  )

(defun bitbake-task-log-runfile ()
  "Return the name of the run file matching the current file.
Logfiles have names like \"log.<name>.<number>\".  The matching
run file have a name like \"run.<name>.<number>\".  If the
current buffer is visiting a symlink -- typical case is the
log.<name> link -- the real file name is used to find the matching
run file."
  (let ((name (file-chase-links buffer-file-name)))
	(when name
	  (when (string-match "/\\(log\\|run\\)\\.\\([^/]+\\.[0-9]+\\)$" name)
		(concat (file-name-directory name)
				(if (string= (match-string 1 name) "run") "log" "run")
				"."
				(match-string 2 name))))))

(defvar ff-always-try-to-create)
(defvar ff-quiet-mode)         ;inform compiler that variable exists
(defvar ff-other-file-alist)   ;inform compiler that variable exists
(defvar ff-case-fold-search)   ;inform compiler that variable exists
(defvar ff-ignore-include)	   ;inform compiler that variable exists
(defvar ff-search-directories) ;inform compiler that variable exists

(defun bitbake-task-log-ff-setup ()
  "Set up `ff-find-other-file' functionality for `bitbake-log-mode'."
  ;;in case find-file.el has not been loaded yet we make the variables
  ;;buffer local with `make-local-variable' even if they are specified
  ;;as such in find-file.el.
  (set (make-local-variable 'ff-always-try-to-create) nil);never attempt to create run files
  (set (make-local-variable 'ff-quiet-mode) nil) ;quiet search
  (set (make-local-variable 'ff-other-file-alist) nil) ;related file is not by extension
  (set (make-local-variable 'ff-case-fold-search) t)
  (set (make-local-variable 'ff-ignore-include) t)
  (set (make-local-variable 'ff-search-directories) '("." "/"))
  (set (make-local-variable 'ff-special-constructs) '(("" . bitbake-task-log-runfile))))


;;;###autoload
(define-generic-mode bitbake-task-log-mode nil nil
					 bitbake-task-log-mode-font-lock-keywords
					 '("log.do_.*\\'")
                     '(view-mode bitbake-task-log-ff-setup buffer-disable-undo
                                 bitbake-task-log-specifics)
					 "Mode for bitbake task log files."
					 )

(defface bitbake-task-log-configure-cached
  '((default :weight bold))
  "Face for highlighting `(cached)' in `log.do_configure' buffers."
  :group 'bitbake-log)

(defface bitbake-task-log-configure-yes
  '((default :foreground "green4" :weight bold))
  "Face for highlighting `yes' results in `log.do_configure' buffers."
  :group 'bitbake-log)

(defface bitbake-task-log-configure-no
  '((default :foreground "red" :weight bold))
  "Face for highlighting `no' results in `log.do_configure' buffers."
  :group 'bitbake-log)

(defface bitbake-task-log-configure-other
  '((default :foreground "gold4" :weight bold))
  "Face for highlighting text results in `log.do_configure' buffers."
  :group 'bitbake-log)

(defun bitbake-log-looking-at-yes (bound)
  "Return t if text after point is `yes' and ends before BOUND.
This function modifies the match data that ‘match-beginning’,
‘match-end’ and ‘match-data’ access; save and restore the match
data if you want to preserve them."
  (search-forward "yes" (min bound (+ (point) 3)) t))

(defun bitbake-log-looking-at-no (bound)
  "Return t if text after point is `no' and ends before BOUND.
This function modifies the match data that ‘match-beginning’,
‘match-end’ and ‘match-data’ access; save and restore the match
data if you want to preserve them."
  (search-forward "no$" (min bound (+ (point) 2)) t))

(defvar bitbake-task-log-do-configure-font-lock-list
  `(("\\.\\.\\. \\((cached) \\)?" (1 'bitbake-task-log-configure-cached nil t)
     (bitbake-log-looking-at-yes nil nil (0 'bitbake-task-log-configure-yes))
     (bitbake-log-looking-at-no nil nil (0 'bitbake-task-log-configure-no))
     (".*" nil nil (0 'bitbake-task-log-configure-other))))
  "Extra expressions for `font-lock' to highlight for `do_configure' logs.
Each element will be appended to `font-lock-keywords' in any
`log.do_configure' buffers.")

(define-minor-mode bitbake-task-log-configure-mode
  "Minor mode for font locking in log.do_configure buffers."
  :group 'bitbake
  (if bitbake-task-log-configure-mode
      (font-lock-add-keywords nil bitbake-task-log-do-configure-font-lock-list)
    (font-lock-remove-keywords nil bitbake-task-log-do-configure-font-lock-list)))

(defun bitbake-task-log-specifics ()
  "Do task specific setup for `bitbake-task-log-mode'."
  (let ((task (replace-regexp-in-string
               "^log.do_\\(.*\\)\\(?:\\.[0-9]+\\)?" "\\1"
               (file-name-nondirectory (buffer-file-name)))))
    (pcase task
      ("configure" (bitbake-task-log-configure-mode))
      ("compile" (compilation-minor-mode))
      ("compile_kernelmodules" (compilation-minor-mode)))))

;;;the auto-mode-alist entry can be created by define-generic-mode,
;;;but then no autoload entry will be created
;:;###autoload
(add-to-list 'auto-mode-alist '("log.do_.*\\'" . bitbake-task-log-mode))

;;; Section: bitbake run files

(defgroup bitbake-run nil
  "Customization options for bitbake-run-file-modes."
  :group 'bitbake)

(defcustom bitbake-run-file-hook '(view-mode bitbake-task-log-ff-setup)
  "Hooks that are run at the end of `bitbake-*-run-file-mode'."
  :group 'bitbake-run
  :options '(view-mode bitbake-task-log-ff-setup)
  :type 'hook)

(define-derived-mode bitbake-python-run-file-mode python-mode "bb-run-python"
  "Mode for bitbake run files for python tasks."
  (run-hooks 'bitbake-run-file-hook))

(define-derived-mode bitbake-shell-run-file-mode sh-mode "bb-run-sh"
  "Mode for bitbake run files for shell tasks."
  (run-hooks 'bitbake-run-file-hook))

;;;###autoload
(defun bitbake-run-file-mode-selector ()
  "Select the proper `bitbake-*-run-file-mode' for buffer.
Unfortunately this mode selection function will not be called on
run files as the interpreter specification will trigger first and
select `sh-mode'."
  (if (save-excursion
		(save-restriction
		  (widen)
		  (goto-char (point-min))
		  (not (looking-at-p "#!/.*sh"))))
	  (bitbake-python-run-file-mode)
	(bitbake-shell-run-file-mode)))

;;;###autoload
(add-to-list 'auto-mode-alist '("run\\..*\\.[0-9]+\\'" . bitbake-run-file-mode-selector))
;;do not autoload bitbake-find-hook and the add-hook call as that
;;would trigger immediate load for bitbake.el. Assume that we do not
;;need the bitbake-find-file-hook until bitbake.el has been loaded for
;;some other reason.
(defun bitbake-find-file-hook ()
  "Extra mode switch point for bitbake files.
Bitbake uses files which are hard to recognize as bitbake files
as they use filenames that match other major modes or have an
interpreter string that matches `interpreter-mode-alist'.  This
hook function gives one last chance to recognize bitbake files
and switch to the correct mode."
  (cond ((and (eq major-mode 'sh-mode)
			  (string-match "run\\..*\\.[0-9]+\\'" buffer-file-name))
		 (bitbake-shell-run-file-mode))))
(add-hook 'find-file-hook 'bitbake-find-file-hook)

(defvar conf-bitbake-mode-syntax-table
  (let ((table (make-syntax-table conf-mode-syntax-table)))
	(modify-syntax-entry ?\# "<" table) ;# starts comments
    (modify-syntax-entry ?\; "." table) ;override, ; is not a comment starter
	table)
  "Syntax table in use in bitbake style `conf-mode' buffers.")

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.scc\\'" . bb-scc-mode))

(provide 'bitbake)
;; Local Variables:
;; tab-width: 4
;; End:
;;; bitbake.el ends here

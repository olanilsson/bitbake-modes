;;; bitbake-modes.el --- Major modes for editing bitbake bb* and conf files. -*- lexical-binding: t -*-

;; Copyright (C) 2012-2024 Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Maintainer: Ola Nilsson <ola.nilsson@gmail.com>
;; Created: 4 Dec 2012
;; Version: 0.5.4
;; Keywords: conf bitbake languages
;; Homepage: https://bitbucket.org/olanilsson/bitbake-modes
;; Package-Requires: ((emacs "25.1") (cl-lib "0.5") (mmm-mode "0.5.4"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Major modes to edit bitbake and associated files.

;; The `bitbake-mode' and `conf-bitbake-mode' modes are designed to
;; edit the various configuration, recipe, and class files used by
;; Bitbake.

;; `bitbake-mode' is meant for .bb, .bbclass, and .bbappend files,
;; while `conf-bitbake-mode' is meant for .conf files.  Strictly
;; speaking `bitbake-mode' can also be used for .conf files, but
;; `conf-bitbake-mode' is more lightweight as it does not use
;; `mmm-mode'.

;;; Code:

(require 'bitbake)

(provide 'bitbake-modes)
;;; bitbake-modes.el ends here

;;; bitbake-font-lock-test.el --- tests for bitbake font lock  -*- lexical-binding: t; -*-

;; Copyright (C) 2017-2018,2022,2025  Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Regression tests for bitbake modes fontification.

;;; Code:

(require 'faceup)
(require 'conf-bitbake-mode)
(require 'ert)
;; ert-font-lock was added in Emacs 30, don't error if it is not found
(unless (and (require 'ert-font-lock nil t)
             (fboundp 'ert-font-lock-deftest-file))
  (defmacro ert-font-lock-deftest-file (name &rest _docstring-keys-mode-and-file)
    `(ert-set-test ',name
                   (make-ert-test
                    :name ',name
                    :body (lambda () (ert-skip "ert-font-lock not available"))))))


(defun bitbake-test-conf-font-lock (faceup)
  "Verify a FACEUP string in `conf-bitbake-mode'."
  (faceup-test-font-lock-string 'conf-bitbake-mode faceup))

(defun bitbake-test-font-lock (faceup)
  "Verify a FACEUP string in `conf-bitbake-mode'."
  (faceup-test-font-lock-string 'bitbake-mode faceup))

(faceup-defexplainer bitbake-test-conf-font-lock)
(faceup-defexplainer bitbake-test-font-lock)

(defvar bitbake-test-faceup-file-dir (concat (faceup-this-file-directory) "files/")
  "Directory for faceup test files.")

(defun bitbake-test-conf-font-lock-file (file)
  "Test that FILE is fontified according to FILE.faceup."
  (faceup-test-font-lock-file 'conf-bitbake-mode
							  (concat bitbake-test-faceup-file-dir file)))
(faceup-defexplainer bitbake-test-conf-font-lock-file)

(ert-deftest bitbake-test-font-lock-variable-1 ()
  "Verify font lock of various variable assignment types."
  (should (bitbake-test-conf-font-lock "«k:export» «v:FOO» = «s:\"foo\"»\n"))
  (should (bitbake-test-conf-font-lock "«k:export» «v:FoO» := «s:\"foo\"»\n"))
  (should (bitbake-test-conf-font-lock "«k:export» «v:F1O» ?= «s:\"foo\"»\n"))
  (should (bitbake-test-conf-font-lock "«k:export» «v:F~O» ??= «s:\"foo\"»\n"))
  (should (bitbake-test-conf-font-lock "«v:F_O» .= «s:\"foo\"»\n"))
  (should (bitbake-test-conf-font-lock "«v:FO+» =. «s:\"foo\"»\n"))
  (should (bitbake-test-conf-font-lock "«v:F.O» += «s:\"foo\"»\n"))
  (should (bitbake-test-conf-font-lock "«v:$OO» =+ «s:'foo'»\n"))
  (should (bitbake-test-conf-font-lock "«k:export» «v:F{OO}/»")))

(ert-deftest bitbake-test-font-lock-include-1 ()
  "Verify font lock of require and include."
  (should (bitbake-test-conf-font-lock "«k:include» foobar"))
  (should (bitbake-test-conf-font-lock " include foobar"))
  (should (bitbake-test-conf-font-lock "«k:include_all» foobar barfoo"))
  (should (bitbake-test-conf-font-lock " include foobar"))
  (should (bitbake-test-conf-font-lock "«k:require» foobar"))
  (should (bitbake-test-conf-font-lock " require foobar")))

(ert-deftest bitbake-test-font-lock-example-01-inc ()
  "Verify font-lock for example_01.inc."
  (should (bitbake-test-conf-font-lock-file "example_01.inc")))

(ert-deftest bitbake-test-font-lock-inherit-1 ()
  "Verify font-lock of `inherit' and `inherit_defer' statements."
  (should (bitbake-test-font-lock "«k:inherit»"))
  (should (bitbake-test-font-lock "«k:inherit_defer»"))
  (should (bitbake-test-font-lock "«k:inherit» «t:someclass»"))
  (should (bitbake-test-font-lock "«k:inherit_defer» «t:someclass»"))
  (should (bitbake-test-font-lock "«k:inherit» «t:someclass» «t:someclass»"))
  (should (bitbake-test-font-lock "«k:inherit_defer» «t:someclass» «t:someclass»"))
  (should (bitbake-test-font-lock "«k:inherit» «t:${VaRiABLE}»"))
  (should (bitbake-test-font-lock "«k:inherit_defer» «t:some${VAR}class»")))

(ert-deftest bitbake-test-font-lock-addfragments-1 ()
  "Verify font-lock of `addfragments'."
  (should (bitbake-test-font-lock "«k:addfragments»"))
  (should (bitbake-test-font-lock "«k:addfragments» some/path «v:VARNAME»")))

(ert-font-lock-deftest-file bitbake-test-font-lock-1
    "Test font-lock for a configuration type bitbake file."
  bitbake-mode "conf-example.conf")


(provide 'bitbake-font-lock-test)
;;; bitbake-font-lock-test.el ends here

# this is a comment
do_things(){
	: ${foo:=bar}
}

python() {
    # not a shell function
}

python nonanon (){
    a = 1 + 2
}

fakeroot OTHER_FUNCTION () {
	bbwarn This is the OTHER_FUNCTION
}

def foobar(d, q, w, e, s):
    pass

# end of file

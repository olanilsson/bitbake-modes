;;; conf-bitbake-mode-test.el --- Tests for conf-bitbake-mode.el  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'conf-bitbake-mode)

(ert-deftest bitbake-test-conf-p-1 ()
  "Test `bitbake-test-conf-p'."
  (let ((buffer-file-name "XXXbitbakeXXX"))
	(should-not (bitbake-conf-p)))
  (let ((buffer-file-name "meta-foobar/conf/layer.conf"))
	(should (bitbake-conf-p)))
  (let ((buffer-file-name "meta-fubar/recipes-snafu/whatever/files/somefile.conf"))
	(should-not (bitbake-conf-p)))
  (let ((buffer-file-name "scripts/foobar/something/xorg.conf"))
	(should-not (bitbake-conf-p)))
  )

(provide 'conf-bitbake-mode-test)
;;; conf-bitbake-mode-test.el ends here

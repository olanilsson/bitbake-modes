;;; bb-test-common.el --- Common parts of bitbake tests

;; Copyright (C) 2014-2017  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Test helpers for bitbake-modes tests and backported functions and
;; macros.

;;; Code:

(require 'cl-lib)
(eval-and-compile
  (require 'ert-x)
  (require 'macroexp))


(unless (fboundp 'take)
  (defun take (n list)
    "Return the first N elements of LIST.
If N is zero or negative, return nil.
If N is greater or equal to the length of LIST, return LIST (or a copy).
Backport from Emacs 29.1, where it is a C function."
    (cl-loop for i from 1 to n
             collect (pop list))))

(defun bb--macroexp-file-name ()
  "Return the name of the file from which the code comes.
Returns nil when we do not know.
A non-nil result is expected to be reliable when called from a macro in order
to find the file in which the macro's call was found, and it should be
reliable as well when used at the top-level of a file.
Other uses risk returning non-nil value that point to the wrong file.
Backported from Emacs 29.1."
  ;; `eval-buffer' binds `current-load-list' but not `load-file-name',
  ;; so prefer using it over using `load-file-name'.
  (let ((file (car (last current-load-list))))
    (or (if (stringp file) file)
        (bound-and-true-p byte-compile-current-file))))
(unless (fboundp 'macroexp-file-name)
  (defalias 'macroexp-file-name 'bb--macroexp-file-name))

(defmacro bb--ert-resource-directory ()
  "Return absolute file name of the resource (test data) directory.

The path to the resource directory is the \"resources\" directory
in the same directory as the test file this is called from.

If that directory doesn't exist, find a directory based on the
test file name.  If the file is named \"foo-tests.el\", return
the absolute file name for \"foo-resources\".

Backport from Emacs 29.1, where it supports customizable naming schemes."
  `(let ((testfile ,(or (macroexp-file-name)
                        buffer-file-name)))
     (when testfile
     (let ((default-directory (file-name-directory testfile)))
       (file-truename
        (if (file-accessible-directory-p "resources/")
            (expand-file-name "resources/")
          (expand-file-name
           (format "%s-resources/"
                   (string-trim testfile "" "\\(-tests?\\)?\\.el")))))))))
(unless (fboundp 'ert-resource-directory)
  (defalias 'ert-resource-directory 'bb--ert-resource-directory))


(defmacro bb--ert-resource-file (file)
  "Return absolute file name of resource (test data) file named FILE.
A resource file is defined as any file placed in the resource
directory as returned by `ert-resource-directory'.
Backport from Emacs 29.1."
  `(expand-file-name ,file (ert-resource-directory)))
(unless (fboundp 'ert-resource-file)
  (defalias 'ert-resource-file 'bb--ert-resource-file))


(defmacro bb-with-named-temp-buffer (&rest body)
  "Execute BODY in a buffer named as the current `ert' test.
The buffer is created if it doesn't exist.  If it does exist is
is cleared of all content and switched to fundamental mode."
  (declare (indent 0) (debug t))
  `(let* ((bufname (concat "*" (symbol-name (ert-test-name (ert-running-test))) "*"))
		  (buf (get-buffer bufname)))
	 (when buf (kill-buffer buf))
	 (with-current-buffer (get-buffer-create bufname)
	   ,@body
	   )))

(cl-defun bb-ert-resource-file-contents (file &key (start 0) (end 0))
  "Return the contents of resource FILE as s string.
Remove START characters from the beginning of the contents and
END characters from the end."
  (with-temp-buffer
    (insert-file-contents (ert-resource-file file))
    (buffer-substring (1+ start) (- (point-max) end))))


(provide 'bb-test-common)
;;; bb-test-common.el ends here

;;; bitbake-mmm-test.el --- Test bitbake mmm integration  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Axis Communications AB

;; Author: Ola x Nilsson <ola.x.nilsson@axis.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'cl-lib)
(require 'mmm-mode)
(eval-and-compile
  (require 'ert-x)
  (require 'bb-test-common))

(ert-deftest bitbake-test-mmm-def-region-1 ()
  :tags '(bitbake mmm)
  (ert-with-test-buffer ()
    (insert-file-contents (ert-resource-file "python-defs.bb"))

    (setq-local mmm-classes 'bitbake-python-def)
    (let ((noninteractive nil))
      (mmm-mode-on)
      (let ((regions (mmm-regions-in (point-min) (point-max))))
        (should (equal (mapcar (lambda (mr) (take 3 mr)) regions)
                       '((fundamental-mode 1 21)
                         (python-mode 21 75)
                         (fundamental-mode 75 78)
                         (python-mode 78 116)
                         (fundamental-mode 116 164))))
        (cl-destructuring-bind (_mode beg end ov) (nth 1 regions)
          (should (string= (bb-ert-resource-file-contents "python-defs-function.py")
                           (buffer-substring-no-properties beg end)))
          (ert-info ((pp-to-string (overlay-properties ov)))
            (should (equal (plist-get (overlay-properties ov) 'mmm-mode) 'python-mode))))
        (cl-destructuring-bind (_mode beg end ov) (nth 3 regions)
          (should (string= (bb-ert-resource-file-contents "python-defs-another.py")
                           (buffer-substring-no-properties beg end)))
          (ert-info ((pp-to-string (overlay-properties ov)))
            (should (equal (plist-get (overlay-properties ov) 'mmm-mode) 'python-mode))))
        ))))

(ert-deftest bitbake-test-mmm-python-region-1 ()
  :tags '(bitbake mmm)
  (ert-with-test-buffer ()
    (insert-file-contents (ert-resource-file "python-python.bb"))
    (setq-local mmm-classes 'bitbake-python-python)
    (let ((noninteractive nil))
      (let ((inhibit-message t)) ; Can’t guess python-indent-offset, using defaults: 4
        (mmm-mode-on))
      (let ((regions (mmm-regions-in (point-min) (point-max))))
        (should (equal (mapcar (lambda (mr) (take 3 mr)) regions)
                       '((fundamental-mode 1 40)
                         (python-mode 40 59)
                         (fundamental-mode 59 101)
                         (python-mode 101 126)
                         (fundamental-mode 126 141)
                         (python-mode 141 150)
                         (fundamental-mode 150 166)
                         )))
        (cl-destructuring-bind (_mode beg end ov) (nth 1 regions)
          (should (string= (mapconcat #'identity
                                      '("    pass"
                                        ""
                                        "    pass"
                                        "")
                                      "\n")
                           (buffer-substring-no-properties beg end)))
          (ert-info ((pp-to-string (overlay-properties ov)))
            (should (equal (plist-get (overlay-properties ov) 'mmm-mode) 'python-mode))))
        (cl-destructuring-bind (_mode beg end ov) (nth 3 regions)
          (should (string= "    # just some comments\n"
                           (buffer-substring-no-properties beg end)))
          (ert-info ((pp-to-string (overlay-properties ov)))
            (should (equal (plist-get (overlay-properties ov) 'mmm-mode) 'python-mode))))
        (cl-destructuring-bind (_mode beg end ov) (nth 5 regions)
          (should (string= "    pass\n"
                           (buffer-substring-no-properties beg end)))
          (ert-info ((pp-to-string (overlay-properties ov)))
            (should (equal (plist-get (overlay-properties ov) 'mmm-mode) 'python-mode))))
        ))))

(ert-deftest bitbake-test-mmm-shell-region-1 ()
  :tags '(bitbake mmm)
  (ert-with-test-buffer ()
    (insert-file-contents (ert-resource-file "shell-shell.bb"))
    (setq-local mmm-classes 'bitbake-shell-function)
    (let ((noninteractive nil))
      (let ((inhibit-message t))
        ;; Making sh-shell-file buffer-local while locally let-bound!
        ;; Setting up indent for shell type sh
        ;; Indentation variables are now local.
        ;; Indentation setup for shell type sh
        (mmm-mode-on))
      (let ((regions (mmm-regions-in (point-min) (point-max))))
        (should (equal (mapcar (lambda (mr) (take 3 mr)) regions)
                       '((fundamental-mode 1 21)
                         (bb-sh-mode 21 50)
                         (fundamental-mode 50 102)
                         (bb-sh-mode 102 158)
                         (fundamental-mode 158 173)
                         )))
        (cl-destructuring-bind (_mode beg end ov) (nth 1 regions)
          ;; remove the trailing newline from file
          (should (string= (bb-ert-resource-file-contents "shell-do_things.sh" :end 1)
                           (buffer-substring-no-properties beg end)))
          (ert-info ((pp-to-string (overlay-properties ov)))
            (should (equal (plist-get (overlay-properties ov) 'mmm-mode) 'bb-sh-mode))))
        (cl-destructuring-bind (_mode beg end ov) (nth 3 regions)
          ;; remove the trailing newline from file
          (should (string= (bb-ert-resource-file-contents "shell-OTHER_FUNCTION.sh" :end 1)
                           (buffer-substring-no-properties beg end)))
          (ert-info ((pp-to-string (overlay-properties ov)))
            (should (equal (plist-get (overlay-properties ov) 'mmm-mode) 'bb-sh-mode))))
        ))))

(ert-deftest bitbake-test-mmm-functions-1 ()
  :tags '(bitbake mmm)
  (ert-with-test-buffer ()
    (insert-file-contents (ert-resource-file "functions.bb"))
    (setq-local mmm-classes 'bitbake-function)
    (let ((noninteractive nil))
      (let ((inhibit-message t))
        ;; Making sh-shell-file buffer-local while locally let-bound!
        ;; Setting up indent for shell type sh
        ;; Indentation variables are now local.
        ;; Indentation setup for shell type sh
        (mmm-mode-on))
      (let ((regions (mmm-regions-in (point-min) (point-max))))
        (should (equal (mapcar (lambda (mr) (take 3 mr)) regions)
                       '((fundamental-mode 1 21)
                         (bb-sh-mode 21 50)
                         (fundamental-mode 50 63)
                         (python-mode 63 90)
                         (fundamental-mode 90 112)
                         (python-mode 112 126)
                         (fundamental-mode 126 138)
                         (bb-sh-mode 138 194)
                         (fundamental-mode 194 196)
                         (python-mode 196 232)
                         (fundamental-mode 232 247)
                         )))))))

(provide 'bitbake-mmm-test)
;;; bitbake-mmm-test.el ends here

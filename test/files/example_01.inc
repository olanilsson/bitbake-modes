require conf/machine/include/soc-family.inc

addpylib dir namespace

# The default for ${VENDOR_TARGET_CPU} is set in the <soc>.inc file
VENDOR_TARGET_ARCH ?= "${VENDOR_COMPILER_ARCH}"
VENDOR_TARGET_ARCH_class-native = "host"
VENDOR_TARGET_CPU_class-native = "${BUILD_ARCH}"

MACHINE_EXTRA_RRECOMMENDS += "kernel-modules"

VENDOR_PRODUCT_CONFIG ?= "conf/machine/vendor-config/${MACHINE}.extra_config"
INHERIT += "vendor-config-handler"
IMAGE_CLASSES += "image_fimage vendor-configure"

PREFERRED_PROVIDER_virtual/kernel ?= "linux-example"
PREFERRED_PROVIDER_virtual/bootloader = "myboot"

EXTRA_IMAGEDEPENDS += "extradepends"
IMAGE_FSTYPES ?= "tar.bz2 ext4 ext3"
MACHINE_FEATURES += "ubi"

# Variables included in Yocto BSP confs
PREFERRED_PROVIDER_virtual/xserver ?= "xserver-xorg"
XSERVER = ""
SERIAL_CONSOLE = "115200 ttyS0"
USE_VT ?= "0"

require extras.inc

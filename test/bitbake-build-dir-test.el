;;; bitbake-build-dir-test.el --- Tests for bitbake-build-dir.el -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Tests should be tagged with
;; - `bitbake-build-dir' for the file targeted by tests in this file
;; - the name of the function the test targets, directly or indirectly

;;; Code:

(require 'bitbake-build-dir)
(require 'bb-test-common)

(ert-deftest bitbake-test-layer-dir-1 ()
  "Check that `bitbake--build-dir' finds the layer dir."
  :tags '(bitbake-build-dir bitbake--layer-dir)
  (let ((quilt-bb (ert-resource-file "trees/poky/meta/recipes-devtool/quilt/quilt_0.67.bb")))
    (should (string= (file-truename (bitbake--layer-dir quilt-bb))
                     (file-truename (ert-resource-file "trees/poky/meta/")))))
  (should-not (bitbake--layer-dir (ert-resource-file "trees/poky/build/conf/local.conf"))))

(ert-deftest bitbake-test-build-base-p-1 ()
  :tags '(bitbake-build-dir bitbake--build-base-p)
  (should (bitbake--build-base-p (ert-resource-file "trees")))
  (should (bitbake--build-base-p (ert-resource-file "trees/poky")))
  (should-not (bitbake--build-base-p (ert-resource-file "trees/poky/build")))
  (should-not (bitbake--build-base-p (ert-resource-file "trees/poky/meta")))
  )

(ert-deftest bitbake-test-build-base-dir-1 ()
  :tags '(bitbake-build-dir bitbake--build-base-dir)
  (should-error (bitbake--build-base-dir
                 (ert-resource-file "trees/poky/bitbake/README"))
                :type 'user-error)
  (should (string=
           (expand-file-name
            (bitbake--build-base-dir
             (ert-resource-file "trees/poky/meta/recipes-devtool/quilt/quilt_0.67.bb")))
           (file-name-as-directory (ert-resource-file "trees/poky"))))
  (should-error (bitbake--build-base-dir) :type 'user-error)
  )

(defmacro bb-test-with-widget-choose (selection &rest body)
  "Always return SELECTION from `widget-choose' while executing BODY."
  (declare (indent 1) (debug (selection &rest body)))
  (let ((selsym (cl-gensym)))
    `(let ((,selsym ,selection))
       (cl-letf (((symbol-function 'widget-choose)
                  (lambda (_title items &optional _event)
                    (or (cdr (assoc ,selsym items))
                        (error "%S not found among items %S" ,selsym items)))))
         ,@body))))

(ert-deftest bitbake-test-recipe-build-dir-1 ()
  :tags '(bitbake-build-dir bitbake-recipe-build-dir)
  (bb-test-with-widget-choose (ert-resource-file "trees/poky/build")
    ;; not already selected, file given, pick build
    (let (bitbake-recipe-build-dir)
      (should (string=
               (bitbake-recipe-build-dir
                (ert-resource-file "trees/poky/meta/recipes-devtool/quilt/quilt_0.67.bb"))
               (ert-resource-file "trees/poky/build"))))
    (let (bitbake-recipe-build-dir)
      ;; not already selected, no file given, pick build
      (with-temp-buffer
        (cl-letf (((symbol-function 'buffer-file-name)
                   (lambda ()
                     (ert-resource-file "trees/poky/meta/recipes-devtool/quilt/quilt_0.67.bb"))))
          (should (string=
                   (bitbake-recipe-build-dir)
                   (ert-resource-file "trees/poky/build"))))))
    (let ((bitbake-recipe-build-dir (ert-resource-file "trees/poky/build")))
      ;; build already selected
      (should (string=
               (bitbake-recipe-build-dir
                (ert-resource-file "trees/poky/meta/recipes-devtool/quilt/quilt_0.67.bb"))
               (ert-resource-file "trees/poky/build")))))
  ;; build already selected, force new choice with prefix argument, pick qemuarm
  (bb-test-with-widget-choose (ert-resource-file "trees/poky/builds/qemuarm")
    (let ((bitbake-recipe-build-dir (ert-resource-file "trees/poky/build"))
          (current-prefix-arg '(4)))
      (should (string=
               (bitbake-recipe-build-dir
                (ert-resource-file "trees/poky/meta/recipes-devtool/quilt/quilt_0.67.bb"))
               (ert-resource-file "trees/poky/builds/qemuarm")))))
  ;; no build dir found, ask the user
  (let (bitbake-recipe-build-dir)
    (cl-letf (((symbol-function 'buffer-file-name)
                   (lambda ()
                     (ert-resource-file "shell-shell.bb"))))
      (should-error (bitbake-recipe-build-dir (ert-resource-file "shell-shell.bb")))))
  (let (bitbake-recipe-build-dir)
    (cl-letf (((symbol-function 'bitbake-read-directory-name)
               (lambda (&rest _) "foo"))
              ((symbol-function 'buffer-file-name)
               (lambda ()
                 (ert-resource-file "trees/meta-solo/recipes-x/some-recipe.bb"))))
      (should (string= (bitbake-recipe-build-dir (ert-resource-file "trees/meta-solo/recipes-x/some-recipe.bb"))
                       "foo"))))
  )

(ert-deftest bitbake-test-default-tempdir-candidates-1 ()
  :tags '(bitbake-build-dir bitbake-default-tempdir-candidates)
  (should (cl-tree-equal (bitbake-default-tempdir-candidates "foo")
                         (list "foo/tmp*")
                         :test #'equal)))

(ert-deftest bitbake-test-recipe-build-dir-dired-1 ()
  :tags '(bitbake-build-dir bitbake-recipe-build-dir-dired)
  (let ((bitbake-recipe-build-dir (ert-resource-file "trees/poky/build")))
    (let ((bfr (bitbake-recipe-build-dir-dired
                (ert-resource-file "trees/poky/meta/recipes-devtool/quilt/quilt_0.67.bb"))))
      (with-current-buffer bfr
        (should (eq major-mode 'dired-mode))
        (should (string= dired-directory
                         (abbreviate-file-name
                          (ert-resource-file
                           "trees/poky/build/tmp/work/x86_64-linux/quilt-native/0.67-r0/"))))))))

(provide 'bitbake-build-dir-test)
;;; bitbake-build-dir-test.el ends here

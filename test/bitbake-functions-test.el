;;; bitbake-functions-test.el --- Tests for functions in bitbake-functions.el

;; Copyright (C) 2014-2017  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'bitbake-functions)

(ert-deftest bitbake-test-PN-1 ()
  "Test `bitbake-PN' for filenames with and without path."
  (should (string= "package" (bitbake-PN "package_1.0.bb")))
  (should (string= "package" (bitbake-PN "/foo/bar/baz/package_1.0.bb"))))

(ert-deftest bitbake-test-PN-2 ()
  "Test `bitbake-PN' with `buffer-file-name' with and without path."
  (let ((buffer-file-name "package-native.bb"))
    (should (string= "package-native" (bitbake-PN))))
  (let ((buffer-file-name "/foo/bar/baz/package-native.bb"))
    (should (string= "package-native" (bitbake-PN)))))

(ert-deftest bitbake-test-PN-3 ()
  "Test `bitbake-PN' with nil arg and `buffer-file-name'."
  (let ((buffer-name nil))
    (should (string= "defaultpkgname" (bitbake-PN)))))

(ert-deftest bitbake-test-PV-1 ()
  "Test `bitbake-PV' for filenames with and without path."
  (should (string= "1.0" (bitbake-PV "package.bb")))
  (should (string= "1.0" (bitbake-PV "/foo/bar/package_1.0.bb")))
  (should (string= "1.3.4" (bitbake-PV "another_1.3.4.bb"))))

(ert-deftest bitbake-test-PV-2 ()
  "Test `bitbake-PV' with `buffer-file-name' with and without path."
  (let ((buffer-file-name "package.bb"))
    (should (string= "1.0" (bitbake-PV))))
  (let ((buffer-file-name "/foo/bar/package_1.0.bb"))
    (should (string= "1.0" (bitbake-PV))))
  (let ((buffer-file-name "package_1.3.4.bb"))
    (should (string= "1.3.4" (bitbake-PV)))))

(ert-deftest bitbake-test-PV-3 ()
  "Test `bitbake-PN' with nil arg and `buffer-file-name'."
  (let (buffer-file-name)
    (should (string= "1.0" (bitbake-PV)))))

(ert-deftest bitbake-test-BPN-1 ()
  (should (string= "package" (bitbake-BPN "package-native_1.0.bb")))
  (should (string= "another" (bitbake-BPN "another-cross_2.0.bb")))
  (should (string= "foo" (bitbake-BPN "nativesdk-foo.bb")))
  (should (string= "foo" (bitbake-BPN "lib32-foo.bb")))
  (should (string= "foo" (bitbake-BPN "lib64-foo.bb")))
  (should (string= "foo" (bitbake-BPN "lib64-foo-native.bb"))))

(ert-deftest bitbake-test-BP-1 ()
  (should (string= "package-1.0" (bitbake-BP "package-native_1.0.bb")))
  (should (string= "another-2.0" (bitbake-BP "another-cross_2.0.bb")))
  )

(provide 'bitbake-functions-test)
;;; bitbake-functions-test.el ends here

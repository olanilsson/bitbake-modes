;;; bitbake-test.el --- tests for bitbake.el

;; Copyright (C) 2014-2018  Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(add-to-list 'load-path (file-name-directory (or load-file-name
												 buffer-file-name)) t)
(require 'ert)
(require 'bb-test-common)
(require 'bitbake)
(require 'testcover)
;;(require 'cl)
;;(eval-when-compile (require 'cl))

(ert-deftest bitbake-test--imenu-create-index-1 ()
  "Test that `bitbake--imenu-create-index' finds all python functions."
  :tags '(bitbake--imenu-create-index)
  (bb-with-named-temp-buffer
	(insert "def test1():\n    pass\n\n"
			"def test2():\n    pass\n\n"
			"python test3(){\n    pass\n}\n\n"
			"fakeroot python test4(){\n    pass\n}\n\n"
			"python () {\n    pass\n}\n\n")
	(let ((im (bitbake-imenu-create-index)))
	  (should im)
	  (should (= 5 (length im)))
	  (dolist (c im)
		(should (cl-member (car c) '("test1" "test2" "test3" "test4" "ANONYMOUS") :test #'string=))
	  ))))

(add-to-list 'testcover-1value-functions 'make-local-variable)

(ert-deftest bitbake-test-comment-setup-1 ()
  "Test that `bitbake-comment-setup' sets some vars buffer local."
  :tags '(bitbake-comment-setup)
  (bb-with-named-temp-buffer
	(bitbake-comment-setup)
	(let ((locals (buffer-local-variables)))
	  (should (assq 'comment-start locals))
	  (should (assq 'comment-start-skip locals))
	  (should (assq 'comment-indent-function locals))
	  (should (assq 'comment-style locals))
	  (should (assq 'comment-continue locals)))))

(provide 'bitbake-test)
;;; bitbake-test.el ends here

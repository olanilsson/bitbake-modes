;;; bitbake-ff-test.el ---                           -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'bitbake-ff)

(ert-deftest bitbake-test-ff-class-1 ()
  :tags '(bitbake-ff-class bitbake-ff-select)
  (let ((inherit-line "inherit foo bar baz ")
        bitbake-ff-other-file-start-pos)
    (with-temp-buffer
      (insert inherit-line "\n")
      (dotimes (test-point (length inherit-line))
        (ert-info ((format "Checking pos %d" test-point))
          (goto-char (setq bitbake-ff-other-file-start-pos test-point))
          (let ((classname (bitbake-ff-class)))
            (cond ((< test-point 13)
                   (should (string= classname "foo.bbclass")))
                  ((< test-point 17)
                   (should (string= classname "bar.bbclass")))
                  (t
                   (should (string= classname "baz.bbclass"))))))))))

(ert-deftest bitbake-test-ff-variable-class-1 ()
  :tags '(bitbake-ff-variable-class bitbake-ff-select)
  (let (bitbake-ff-other-file-start)
    (with-temp-buffer
      (insert "BBCLASSEXTEND = \"native\"\n")
      (goto-char (point-min))
      (cl-loop for test-point from (point-min) to (line-end-position)
               do (ert-info ((format "Testing pos %d" test-point))
                    (goto-char (setq bitbake-ff-other-file-start-pos test-point))
                    (should (string= (bitbake-ff-variable-class) "native.bbclass")))))))

(ert-deftest bitbake-test-ff-variable-class-2 ()
  :tags '(bitbake-ff-variable-class bitbake-ff-select)
  (let (bitbake-ff-other-file-start)
    (with-temp-buffer
      (insert "INHERIT += \"foo bar\"\n")
      (goto-char (point-min))
      (cl-loop for test-point from (point-min) to (line-end-position)
               do (ert-info ((format "Testing pos %d" test-point))
                    (goto-char (setq bitbake-ff-other-file-start-pos test-point))
                    (cond ((< test-point 17)
                           (should (string= (bitbake-ff-variable-class) "foo.bbclass")))
                          (t
                           (should (string= (bitbake-ff-variable-class) "bar.bbclass")))
                          ))))))

(ert-deftest bitbake-test-ff-variable-excternalsrc-1-absolute ()
  "Verify `bb-ff-externalsrc' when the value is an absolute path."
  :tags '(bitbake-ff-externalsrc)
  (with-temp-buffer
    (let ((expected (expand-file-name default-directory)))
      (setq expected (replace-regexp-in-string "/[^/]+" "/.." expected))
      (setq expected (concat (substring expected 1) "a/path"))
      (insert "EXTERNALSRC = \"/a/path\"\n")
      (goto-char (point-min))
      (cl-loop for test-point from (point-min) to (line-end-position)
               do (ert-info ((format "Testing pos %d" test-point))
                    (setq bitbake-ff-other-file-start test-point)
                    (should (string= (bitbake-ff-externalsrc) expected)))))))

(ert-deftest bitbake-test-ff-variable-excternalsrc-2-relative ()
  "Verify `bb-ff-externalsrc' when the value is a relative path."
  :tags '(bitbake-ff-externalsrc)
  (with-temp-buffer
    (insert "EXTERNALSRC = \"" default-directory "foo/bar\"\n")
    (goto-char (point-min))
    (cl-loop for test-point from (point-min) to (line-end-position)
             do (ert-info ((format "Testing pos %d" test-point))
                  (setq bitbake-ff-other-file-start test-point)
                  (should (string= (bitbake-ff-externalsrc)
                                   "foo/bar"))))))

(provide 'bitbake-ff-test)
;;; bitbake-ff-test.el ends here

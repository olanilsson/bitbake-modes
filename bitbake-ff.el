;;; bitbake-ff.el --- `ff' support for `bitbake-mode'

;; Copyright (C) 2014-2018  Ola  Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Declarations:

(defvar ff-quiet-mode)         ;inform compiler that variable exists
(defvar ff-other-file-alist)   ;inform compiler that variable exists
(defvar ff-case-fold-search)   ;inform compiler that variable exists
(defvar ff-ignore-include)	   ;inform compiler that variable exists
(defvar ff-search-directories) ;inform compiler that variable exists
;; variables that cannot be 'required' because that would create a require loop
(defvar bitbake-topdir-dominating-file) ;bitbake.el
(defvar bitbake-topdir) ;bitbake.el
;;; Code:

(require 'bitbake-functions)
(require 'find-file)
(require 'grep)
(eval-and-compile (require 'cl-lib))

(defcustom bitbake-other-file-alist
  '(
	;; this pattern doesn't work unless the other-file is already
	;; loaded in a buffer as file name matching is not done with regex
	("_%\\.bbappend\\'" ("\\(?:_.*\\)?\\.bb\\'"))
	("\\.bbappend\\'"  (".bb")))
  "Alist of extensions to find given the current file's extension.
This list should contain the most used extensions before the others,
since the search algorithm searches sequentially through each directory
specified in `ff-search-directories'.  If a file is not found, a new one
is created with the first matching extension."
  :type '(repeat (list regexp (choice (repeat string) function)))
  :group 'bitbake)


(defcustom bitbake-ff-search-directories 'bitbake-poky-layer-dirs
  "Function to set up `ff-search-directories'."
  :group 'bitbake
  :type 'function
  :package-version '(bitbake-modes . "0.2.4")
  )

(defcustom bitbake-ff-search-dirs
  '("meta" "meta-*"
    "meta/classes" "meta/classes-global" "meta/classes-recipe"
    "meta-*/classes" "meta-*/classes-global" "meta-*/classes-recipe")
  "Directories to search for other files with `ff-find-other-file'.
`bitbake-topdir' is prepended to each of these and is also used
as a search dir."
  :group 'bitbake
  :type '(repeat string)
  :link '(variable-link bitbake-topdir)
  ;;options are not supported for repeat string, maybe they will be some day.
  :options '("meta" "meta-*"
             "meta/classes" "meta/classes-global" "meta/classes-recipe"
             "meta-*/classes" "meta-*/classes-global" "meta-*/classes-recipe"))

(make-variable-buffer-local
 (defvar bitbake-ff-other-file-start-pos 1
   "Remember point while doing `ff-other-file'."))


(defun bitbake-ff-pre-find-hook ()
  "Store the value of point in `bitbake-ff-other-file-start-pos'.
Point is lost when calling functions specified in
`ff-special-constructs'.  This function stores point in
`bitbake-ff-other-file-start-pos', a buffer local variable."
  (setq bitbake-ff-other-file-start-pos (point)))

(defun bitbake-ff-file-uri ()
  "Return the file name referred in a `file://' URI.
Use this function as an EXTRACT function in
`ff-special-consructs'.  If more than one URI is listed on the
current line, pick the one point is on."
  (bitbake-ff-select "file://\\(\\(?:\\w\\|[-._]\\)+\\)[ \t]*"
					 bitbake-ff-other-file-start-pos
					 1))

(defun bitbake-ff-externalsrc ()
  "Return the path in an EXTERNALSRC variable relative to `default-directory'."
  (when (looking-at "\\s-*EXTERNALSRC\\(?:_.*\\)?\\s-*=\\s-*\"\\(.*\\)\"")
    (file-relative-name
     (concat (file-remote-p default-directory)
             (match-string 1)))))

(defun bitbake-ff-select (regexp pos group)
  "Find the REGEXP match on the current line where POS is closest to GROUP.
Return the match content of GROUP."
  (let ((pos (or pos (point)))
		(bound (line-end-position))
        name group-match)
	(save-excursion
	  (beginning-of-line)
	  (while (and (not name)
				  (re-search-forward regexp bound t))
        (setq group-match (match-string-no-properties group))
        (when (<= pos (match-end group))
          (setq name group-match)))
	  (unless name
        (setq name group-match)))
	name))

(defun bitbake-poky-layer-dirs ()
  "Find directories where files may be found by `ff'.
The code is based on a set of assumptions on how OpenEmbedded
layer trees are set up.

1) Search for the directory holding the first dominating file of
the filenames in `bitbake-topdir-dominating-file'.

2) In the found directory, expand all directory names (with
wildcards) in `bitbake-ff-search-dirs'.

Return a list that is the concatenation of
`ff-search-directories', the found topdir, and all directories
found in step 2 above."
  (let (bitbake-topdir)
	;;set up bitbake-topdir from bitbake-topdir-dominating-file
	(when (and (buffer-file-name) ;;mmm-mode does something funny here
			   bitbake-topdir-dominating-file) ;LICENSE
	  (let ((dom-list bitbake-topdir-dominating-file))
		(while dom-list
		  (let ((dir (locate-dominating-file (buffer-file-name) (car dom-list))))
			(if dir
				(setq bitbake-topdir dir
					  dom-list nil)
			  (setq dom-list (cdr dom-list)))))))
	(when (and bitbake-topdir (not (string= "" bitbake-topdir)))
	  (append ff-search-directories
			  (list bitbake-topdir)
			  (apply 'append
					 (let ((default-directory bitbake-topdir))
					   (mapcar (lambda (subdir)
								 (file-expand-wildcards subdir t)) bitbake-ff-search-dirs)))))))

(defun bitbake-ff-class ()
  "Return the bbclass file name of a bitbake inherit statement.
Use this function as an EXTRACT function in
`ff-special-constructs'.  If more than one class is listed on the
inherit line, pick the one the point is on.  If point is before
the first class name, pick the first class, if point is between
class names pick the preceding class name."
  (let ((name (bitbake-ff-select
			   "\\(?:inherit[ \t]+\\)?\\([^[:blank:]]+\\)[[:blank:]]*"
			   bitbake-ff-other-file-start-pos
			   1)))
	(when name (concat name ".bbclass"))))

(defun bitbake--ff-find-recipe (recipe-name)
  "Find a recipe file for RECIPE-NAME in `ff-search-directories'.
`ff-search-directories' is searched in order until a file
matching RECIPE-NAME*.bb is found.  As `ff-search-directories'
typically doesn't include the recipe-X directories, each
directory search includes two levels of subdirs (dir, dir/*, and
dir/*/*).  Return the name of the found file relative to the the
directory from `ff-search-directories'."
  ;; TODO: Return early, exit loop once first file is found
  (cl-loop for dir in ff-search-directories
           for matches = (cl-remove-if-not
                          #'file-exists-p
                          (cl-loop for fmt in '("%s/%s*.bb" "%s/*/%s*.bb" "%s/*/*/%s*.bb")
                                   nconc (file-expand-wildcards (format fmt dir recipe-name))))
            when matches return (string-replace (car (file-expand-wildcards dir))
                                                "."
                                                (car matches))))

(defun bitbake-ff-depends ()
  "Return the recipe file name from a DEPENDS assignment.
Use this function as an EXTRACT function in
`ff-special-constructs'.  If more than one recipe name is
assigned to DEPENDS on the current line, pick the one point is
on.  If point is before the first name, pick the first name.  If
point is between names, pick the preceeding name.  In order to
find the correct recipe, all directories in
`ff-search-directories' are searched here once, and then again in
`ff-get-file-name'.  This is because `ff-get-file-name' does not
support wildcards."
  (let ((name (bitbake-ff-select
               (rx (opt (seq "DEPENDS" (* blank)
                             "=" (* blank)
                             ?\" (* blank)))
                   (group-n 1 (+ (not (any blank ?\")))))
			   bitbake-ff-other-file-start-pos
			   1)))
	(when name
      (bitbake--ff-find-recipe name))))

(defcustom bitbake-ff-recipe-variables
  '("DEPENDS")
  "List of regexps bitbake variables containing lists of recipe names.
Used to jump to recipe files from recipe name mentions.  Note
that the regexps should not include overrides unless only
variables with overrides should be matched."
  :type '(repeat regexp)
  :package-version '(bitbake-modes . "0.5.4")
  :group 'bitbake)

(defun bitbake-ff-variable-recipe ()
  "Return the recipe file name from a variable assignment.
Only assignments to variables matching
`bitbake-ff-recipe-variables' are considered.  Use this function
as an EXTRACT function in `ff-special-constructs'.  If more than
one recipe name is assigned to a variable on the current line,
pick the one point is on.  If point is before the first name,
pick the first name.  If point is between names, pick the
preceeding name.  In order to find the correct recipe, all
directories in `ff-search-directories' are searched here once,
and then again in `ff-get-file-name'.  This is because
`ff-get-file-name' does not support wildcards."
  (let ((name (bitbake-ff-select
               (rx-to-string `(seq
                               ;; variable regexes
                               (or (regex ,(mapconcat (lambda (re) (concat "\\(?:" re "\\)"))
                                                     bitbake-ff-recipe-variables
                                                     "\\|")))
                               ;; optional override
                               (optional (or ?: ?_)
                                         (1+ (any "a-z" "A-Z" "0-9" ?- "_+.${}/" ?~ ?:)))
                               (0+ blank)
                               ;; assignment operator
                               (or ":=" "??=" "?=" "+=" "=+" "=." ".=" "=")
                               (0+ blank)
                               (any ?\" ?')
                               (0+ blank)
                               (group-n 1 (1+ (not (any blank ?\"))))
                               (0+ blank)))
               bitbake-ff-other-file-start-pos
               1)))
    (when name
      (bitbake--ff-find-recipe name))))

(defun bitbake-ff-variable-class ()
  "Return the bbclass file name of BBCLASSEXTEND and INHERIT assignments.
Use this function as an EXTRACT function in
`ff-special-constructs'.  If more than one class is assigned to
BBCLASSEXTEND or INHERIT, pick the one the point is on.  If point
is before the first class name, pick the first class, if point is
between class names pick the preceding class name."
  (let ((name (bitbake-ff-select
               (rx (optional (or "BBCLASSEXTEND" "INHERIT") ; variable name
                             (not alnum) (1+ (not (any "\""))) ; at least two characters
                                                               ; TODO: VAR="foo"
                             "\""                           ; quote to start variable value
                             (0+ blank))                    ; any whitespace
                   (group-n 1 (1+ (not (any "\"" blank))))  ; the class name
                   (0+ blank))                              ; possibly trailing whitespace
			   bitbake-ff-other-file-start-pos
			   1)))
	(when name (concat name ".bbclass"))))

(defun bitbake-set-ff-search-directories ()
  "Set `ff-search-directories' for `ff-find-other-file'.
The return value from the function in
`bitbake-ff-search-directories' are written to
`ff-search-directories'.  The current dir ('.') is always
prepended to `ff-search-directories'."
  (set (make-local-variable 'ff-search-directories) '("."))
  (when (string-match-p "\\.bb\\(append\\)?$" buffer-file-name)
	(let ((recipedir (file-name-directory buffer-file-name)))
	  (add-to-list 'ff-search-directories (concat recipedir (bitbake-BPN buffer-file-name)))
	  (add-to-list 'ff-search-directories (concat recipedir (bitbake-BP buffer-file-name)))
	  (add-to-list 'ff-search-directories (concat recipedir "files"))))

  ;; A more heuristic approach to the BPN BP addition above
  ;; (setq ff-search-directories
  ;; 		(append ff-search-directories
  ;; 				(mapcar 'car
  ;; 				(remove-if-not 'cadr ;; remove non-directories
  ;; 							   (directory-files-and-attributes
  ;; 								(file-name-directory buffer-file-name) nil
  ;; 								;; create a regexp that matches files
  ;; 								;; that start with the name of the
  ;; 								;; current directory
  ;; 								(concat (file-name-base
  ;; 										 (substring
  ;; 										  (file-name-directory buffer-file-name) 0 -1))
  ;; 										".*"))))))
  (when (and (boundp 'bitbake-ff-search-directories)
			 bitbake-ff-search-directories)
	(let ((base-dirs (funcall bitbake-ff-search-directories)))
	  (setq ff-search-directories
			(append ff-search-directories base-dirs))
	  (when (string-match-p "\\.bb\\(append\\)?$" buffer-file-name)
		(let ((recipe-dirs (mapcar
							(lambda (dir)
							  (file-expand-wildcards (concat dir "/recipes-*") t)) base-dirs)))
		  (setq ff-search-directories
				(append ff-search-directories
						(mapcar (lambda (d2) (concat d2 "/*"))
								(apply #'append recipe-dirs)))))))))

;;;###autoload
(defun bitbake-ff-setup ()
  "Set up `ff-find-other-file' functionality for `bitbake-mode'."
  (when (require 'find-file nil t)
	(set (make-local-variable 'ff-quiet-mode) nil) ;non-quiet search
	;; file relations when point is not on inherit, require, include...
	(set (make-local-variable 'ff-other-file-alist) 'bitbake-other-file-alist)
	(set (make-local-variable 'ff-case-fold-search) t); ignore case
	(set (make-local-variable 'ff-ignore-include) t)	; ignore #include
	;; special patterns that should be handled
	(set (make-local-variable 'ff-special-constructs)
		 ;; - require and include
		 '(("^[ \t]*\\(?:include\\|require\\)[ \t]+\\(.*\\)" .
			(lambda () (match-string-no-properties 1)))
		   ;; - BBCLASSEXTEND
		   ("^[ \t]*\\(BBCLASSEXTEND\\|INHERIT\\)[^[:alnum:]].*=" . bitbake-ff-variable-class)
		   ;; - inherit
		   ("^[ \t]*inherit[[:blank:]]" . bitbake-ff-class)
		   ;; - DEPENDS
		   ("^[ \t]*DEPENDS.*=" . bitbake-ff-depends)
           ;; - EXTERNALSRC
           ("^[ \t]*EXTERNALSRC.*=" . bitbake-ff-externalsrc)
		   ;; - file://
           (".*file://" . bitbake-ff-file-uri)
           ;; - variable assignments, not flags
           ("\\<[a-zA-Z0-9_+.${}/~:-]+[ \t]*[:?+.]?=" . bitbake-ff-variable-recipe)
           ))
	;; bitbake-ff-class requires point, add a hook to store it.
	(add-hook 'ff-pre-find-hook 'bitbake-ff-pre-find-hook nil t)
	;; set up ff-search-directories in ff-pre-find-hook
	(add-hook 'ff-pre-find-hook 'bitbake-set-ff-search-directories t t)))


(provide 'bitbake-ff)
;; Local Variables:
;; tab-width: 4
;; End:
;;; bitbake-ff.el ends here

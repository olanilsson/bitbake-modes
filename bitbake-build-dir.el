;;; bitbake-build-dir.el --- Functions to find bitbakes build directories -*- lexical-binding: t -*-

;; Copyright (C) 2014-2024  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Find various interesting bitbake directories from a recipe (or
;; sometimes other) file in a layer.
;;
;; The LAYERDIR is the top directory of the current layer, typically
;; located by searching towards the root for a file conf/layer.conf.
;; Use `bitbake--layer-dir' to find the LAYERDIR.
;;

;; Work-in-progress code for jumping from a recipe file to the build
;; dir.  Still needs a lot of work.
;;
;; Issues:
;; - Too slow
;; - User interaction using `widget-choose'

;;; Code:

(require 'bitbake-functions)
(require 'bitbake)
(require 'wid-edit)
(eval-and-compile
  (require 'cl-lib))

(defcustom bitbake-build-dir-patterns
  '("build/conf/local.conf"
    "builds/*/conf/local.conf"
    "*/conf/local.conf"
    "*/*/conf/local.conf")
  "File patterns used by `bitbake-build-base-p' to find the base dir.
Each string should be a file path pattern (glob) that can be used to
verify that a directory is the base directory."
  :group 'bitbake
  :type '(repeat string))

(defun bitbake--layer-dir (from-file)
  "Return the layer directory of the layer containing FROM-FILE.
The layer directory is the directory containing the `conf/layer.conf'
file.  Return nil if no such file is found."
  (locate-dominating-file from-file "conf/layer.conf"))

(defun bitbake--build-base-p (directory)
  "Check if DIRECTORY is a bitbake build base dir.
DIRECTORY is considered a build base if it contains any build
dirs with local.conf files.  If the globs `*/conf/local.conf' or
`*/*/conf/local.conf' matches any files in DIRECTORY, the list of
matching files is returned."
  ;;TODO: Use a defcustom for the globs
  ;; Avoid excessive file searches by searching common locations first
  (let ((default-directory directory))
    (cl-loop for pattern in bitbake-build-dir-patterns
             if (file-expand-wildcards pattern) return t)))

(defun bitbake--build-base-dir (&optional from-file)
  "Find the bitbake base directory.
If FROM-FILE is non-nil, it is used as the start point of the search
instead of the file visited by the current buffer.  First search toward
the file system root for a file `conf/layer.conf', the layer root.
Search from the layer root's parent for a directory containing a file
matching any pattern in `bitbake-build-dir-patterns'.  Will signal a
`user-error' unless FROM-FILE is in a bitbake layer."
  (unless (setq from-file (or from-file (buffer-file-name)))
    (user-error "Current buffer is not visiting a file"))
  (let* ((layer-dir (or
                     (bitbake--layer-dir from-file)
                     (user-error "%s is not in a bitbake layer" from-file))))
    (locate-dominating-file (expand-file-name ".." layer-dir) #'bitbake--build-base-p)))

(defun bitbake-read-directory-name (prompt &optional dir default-dirname mustmatch initial
										   predicate)
  "Read directory name, prompting with PROMPT and completing in directory DIR.
Works just as `read-directory-name' for PROMPT, DIR, DEFAULT-DIRNAME,
MUSTMATCH, and INITIAL, but any completion candidate must satisfy both
`file-directory-p' and PREDICATE so more explicit tests can be made."
  (read-file-name prompt dir (or default-dirname
				 (if initial (expand-file-name initial dir)
				   dir))
		  mustmatch initial
                  (if predicate
                      (lambda (filename) (and (file-directory-p filename)
                                              (funcall predicate filename)))
                    #'file-directory-p)))

(defvar-local bitbake-recipe-build-dir nil
  "The recipe build dir can be stored here.
Note that a recipe can be used from several builds, and so the
build directory may be only one among several possible build
directories.")

(defun bitbake-recipe-build-dir (&optional bb-file)
  "Find and return the recipe BUILDDIR for BB-FILE.
Use a set of assumptions to find a list of candidate directories,
and then let the user choose a BUILDDIR.  If no candidates are found,
let the user choose a directory which contain a `conf/local.conf' file.
Cache the selected BUILDDIR in the buffer local variable
`bitbake-recipe-build-dir'.  If variable `bitbake-recipe-build-dir' is
already set and `current-prefix-arg' is not, return the value of that
variable."
  (if (and bitbake-recipe-build-dir
		   (not current-prefix-arg))
	  bitbake-recipe-build-dir
	;; the build dir is unknown, figure it out
	(or bb-file (setq bb-file (buffer-file-name)))
    ;; Find possible BUILDDIRs
	(let* ((build-base (expand-file-name (bitbake--build-base-dir bb-file)))
           (build-dir-patterns '("*/conf/local.conf"
                                 "builds/*/conf/local.conf"
                                 "conf/local.conf"))
		   (local-confs
            (cl-loop for lc in build-dir-patterns
                     with default-directory = build-base
                     for sorted-files = (cl-merge
                                         'list sorted-files
                                         ;; (length "/conf/local.conf") => 16
                                         (mapcar (lambda (s)
                                                   (substring s 0 -16))
                                                 (file-expand-wildcards lc t))
                                         #'string<)
                     finally return sorted-files)))
	  (setq bitbake-recipe-build-dir
			(if local-confs
                (widget-choose "Select a build directory"
                               (cl-mapcar #'cons local-confs local-confs))
			  (bitbake-read-directory-name
               "Select build dir (where `conf/local.conf' is located): "
			   nil nil nil nil
			   (lambda (filename)
                 (let ((default-directory filename))
                   (file-exists-p "conf/local.conf")))))))))

(defun bitbake-default-tempdir-candidates (builddir)
  "Return a list of globs that could match a bitbake TMPDIR for BUILDDIR.
BUILDDIR will be the chosen bitbake BUILDDIR, typically selected
with function `bitbake-recipe-build-dir'."
  (list (format "%s/tmp*" builddir)))

(defcustom bitbake-tempdir-candidates-function #'bitbake-default-tempdir-candidates
  "Function that returns globs of TMPDIR candidates.
See `bitbake-default-tempdir-candidates' for an example."
  :type 'function
  :group 'bitbake)

;;;###autoload
(defun bitbake-recipe-build-dir-dired (&optional bb-file)
  "Open a `dired' buffer on the WORKDIR directory of BB-FILE.
This function uses a lot of heuristics and assumptions and can
probably be very wrong."
  (interactive)
  (let* ((bb-file (or bb-file (buffer-file-name)))
		 (base (bitbake-recipe-build-dir bb-file))
         (tmpdir-candidates (funcall bitbake-tempdir-candidates-function base))
         (pn (bitbake-PN bb-file))
         (bpn (bitbake-BPN bb-file))
         (pv (bitbake-PV bb-file))
		 (matching-version-dirs
          (cl-loop for glob in tmpdir-candidates
                   nconc (file-expand-wildcards (format "%s/work/*/%s/%s*" glob bpn pv))
                   nconc (file-expand-wildcards (format "%s/work/*/%s-native/%s*" glob bpn pv))))
		 (found (mapconcat 'identity matching-version-dirs "\n")))
	(if matching-version-dirs
        (dired
         (if (= 1 (length matching-version-dirs))
             (car matching-version-dirs)
           (widget-choose (format "Multiple matches in %s, pick one" base)
                          (mapcar
                           (let ((prefix-len (1+ (length base))))
                             (lambda (workdir)
                               (cons (substring workdir prefix-len)
                                     workdir)))
                           matching-version-dirs))))
	  ;;possibly we searched for the wrong version....
	  (message "Version directory not found, trying package name directory...")
      (let ((package-dirs
             (cl-loop for glob in tmpdir-candidates
                      nconc (file-expand-wildcards (format "%s/work/*/%s" glob bpn))
                      nconc (file-expand-wildcards (format "%s/work/*/%s-native" glob bpn)))))
        (setq found (car package-dirs)))
	  (if (not (string= found ""))
		  (dired found)
        (message "no build dir found for %s (%s)" pn (file-name-nondirectory bb-file))))))

(provide 'bitbake-build-dir)
;;; bitbake-build-dir.el ends here

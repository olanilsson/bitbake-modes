;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((emacs-lisp-mode . ((eval . (setq flycheck-emacs-lisp-load-path
                                   (cons "." (append (directory-files ".emacs/elpa/") load-path))))
                     (byte-compile-warnings . (not cl-functions))
                     (tab-width . 4)
                     (eval . (add-hook 'before-save-hook #'copyright-update))))
 (lisp-data-mode . ((indent-tabs-mode . nil))))

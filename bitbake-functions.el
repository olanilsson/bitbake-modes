;;; bitbake-functions.el --- BitBake utility functions

;; Copyright (C) 2014-2018  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains functions that emulate BitBake variable values
;; in recipes.  Some variables are ubiquitous in BitBake recipes, and
;; are useful also when working with recipes as they may be used in
;; `inherit', `include' and `require' statements for instance.  So far
;; there is no generic function to extract bitbake varaibles values, but
;; functions are added as variables are needed by other `bitbake-mode'
;; code.

;;; Code:

(defun bitbake-PN (&optional recipe-file)
  "Emulate the BitBake PN variable for RECIPE-FILE.
PN refers to a recipe name in the context of a file used by the
OpenEmbedded build system as input to create a package.  It
refers to a package name in the context of a file created or
produced by the OpenEmbedded build system.  If RECIPE-FILE is
nil, use variable `buffer-file-name'."
  (let ((filename (or recipe-file buffer-file-name)))
	(if filename
		(car (split-string (file-name-sans-extension
							(file-name-nondirectory filename)) "_"))
	  "defaultpkgname")))

(defun bitbake-PV (&optional recipe-file)
  "Emulate the BitBake PV variable for RECIPE-FILE.
The version of the recipe.  The version is normally extracted
from the recipe filename.  If RECIPE-FILE is nil, use variable
`buffer-file-name'."
  (setq recipe-file (or recipe-file buffer-file-name))
  (if recipe-file
	  (or
	   (nth 1
			(split-string (file-name-sans-extension
						   (file-name-nondirectory recipe-file)) "_"))
	   "1.0")
	"1.0"))

(defun bitbake-BPN (&optional recipe-file)
  "Emulate the BitBake BPN variable for RECIPE-FILE.
The bare name of the recipe.  This variable is a version of the
PN variable but removes common suffixes and prefixes, such as
nativesdk-, -cross, -native, and multilib's lib64- and lib32-.
If RECIPE-FILE is nil, use variable `buffer-file-name'."
  (let ((suffixes '("-native" "-cross" "-initial" "-intermediate" "-crosssdk" "-cross-canadian"))
		(prefixes '("nativesdk-" "lib32-" "lib64-"))
		(var (bitbake-PN recipe-file)))
	(dolist (suffix suffixes var)
	  (setq var (replace-regexp-in-string (concat suffix "$") "" var)))
	(dolist (prefix prefixes var)
	  (setq var (replace-regexp-in-string (concat "^" prefix) "" var)))))

(defun bitbake-BP (&optional recipe-file)
  "Emulate the BitBake BP variable for a RECIPE-FILE.
The base recipe name and version but without any special recipe
name suffix (i.e. -native, lib64-, and so forth).  BP is
comprised of `${BPN}-${PV}'.  If RECIPE-FILE is nil, use
variable `buffer-file-name'."
  (concat (bitbake-BPN recipe-file) "-" (bitbake-PV recipe-file)))

(provide 'bitbake-functions)
;; Local Variables:
;; tab-width: 4
;; End:
;;; bitbake-functions.el ends here

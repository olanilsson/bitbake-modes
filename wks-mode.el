;;; wks-mode.el --- Mode for editing Yocto Wic kickstart files.  -*- lexical-binding: t; -*-

;; Copyright (C) 2016-2018  Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords: yocto, kickstart, wic

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'bitbake-compat)

(defgroup bitbake-wic nil "Customization options for wic." :group 'bitbake)

(defun wks-mode-font-lock-keywords ()
  "Return the default font lock keywords for `wks-mode2'."
   `("part" "bootloader" "include" "long-description" "short-description")
   )

;;;###autoload
(define-derived-mode wks-mode prog-mode "wks"
  :group 'bitbake-wic
  (set (make-local-variable 'comment-start) "#")
  (set (make-local-variable 'comment-start-skip) "#+[ \t]*")
  (set (make-local-variable 'comment-indent-function) 'comment-indent-default)
  (set (make-local-variable 'comment-style) 'plain)
  (set (make-local-variable 'comment-continue) nil)
  (setq-local font-lock-defaults '(wks-mode-font-lock-keywords))
  )

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.wks\\'" . wks-mode))

(modify-syntax-entry ?# "<" wks-mode-syntax-table)
(modify-syntax-entry ?\n  ">" wks-mode-syntax-table)
(modify-syntax-entry ?\^m ">" wks-mode-syntax-table)
(modify-syntax-entry ?\"  "\""  wks-mode-syntax-table) ;strings are delimited by "

(provide 'wks-mode)
;;; wks-mode.el ends here

;;; bitbake-conf-mode.el --- Possibly obsolete conf-bitbake-mode -*- lexical-binding: t; -*-

;; Copyright (C) 2014-2025  Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords: tools, extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; `conf-bitbake-mode' is derived from `conf-mode' and meant for
;; BitBake .conf and .inc files.  Strictly speaking `bitbake-mode' can
;; also be used for .conf files, but `conf-bitbake-mode' is more
;; lightweight as it does not use `mmm-mode'.

;; Maybe it would be smarter to use a bitbake-basic-mode as a middle
;; step between `prog-mode' and `bitbake-mode'.  We do not actually
;; use any of the stuff from `conf-mode'.

;;; Declarations:

(declare-function bitbake-ff-setup "bitbake" ())

;;; Code:

;;add this pattern to the front of `auto-mode-alist' so bitbake has a
;;chance to trigger.
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.conf\\'" . conf-bitbake-mode-maybe))
;;;###autoload
(add-to-list 'auto-mode-alist '("\\.inc\\'" . conf-bitbake-mode))
;;;###autoload
(add-to-list 'magic-mode-alist '(bitbake-magic-match-p . bitbake-mode))

(require 'conf-mode)
(defvar conf-bitbake-mode-syntax-table
  (let ((table (make-syntax-table conf-mode-syntax-table)))
	(modify-syntax-entry ?\# "<" table) ;# starts comments
    (modify-syntax-entry ?\; "." table) ;override, ; is not a comment starter
	table)
  "Syntax table in use in bitbake style `conf-mode' buffers.")

;;;###autoload
(defvar conf-bitbake-font-lock-keywords
  `(;; [section] (do this first because it may look like a parameter)
    ("^[ \t]*\\[\\(\\(?:.\\)+\\)\\]" 1 'font-lock-type-face)
    "^include\\(_all\\)?\\>"
	"^require\\>"
    "^addpylib\\>"

    ;; addfragments path-prefix frag-var
    ,(rx bol "addfragments" eow)
    (,(rx bol "addfragments" eow
          (+ blank) (+ (not blank)) (+ blank) ;; ws path ws
          (group-n 1 (+ (not blank))))
     1 font-lock-variable-name-face)

	;; the variable parsing regexp in bitbake is defined in
	;; bitbake/lib/bb/parse/parse_py/ConfHandler.py as
	;; __config__regexp__.

    ;; var=val or var[flag]=val
    (,(concat "^\\(?:\\<\\(export\\)[ \t]+\\)?" ;export keyword
              "\\([-a-zA-Z0-9_+.${}/~:]+?\\)" ;variable name
              "\\(?:\\[\\([-a-zA-Z0-9_+./]+\\)\\]\\)?" ;flag-index-like-op
			  "[ \t]*" ;optional whitespace
			  "\\([:+.]=\\|=[.+]?\\|\\?\\??=\\)")  ;assignment operator
     (1 'font-lock-keyword-face nil t)
     (2 'font-lock-variable-name-face)
     (3 'font-lock-constant-face nil t))
	;; export var
    (,(concat "^\\(\\<export\\|unset\\)[ \t]+" ;export and unset keywords
			  "\\([-a-zA-Z0-9~_+.${}/]+\\)") ;variable name
     (1 'font-lock-keyword-face)
     (2 'font-lock-variable-name-face))
	;;("^[^#\n]+\\(#.*$\\)" 1 'font-lock-warning-face t)
    ;; section { ... } (do this last because some assign ...{...)
    ;("^[ \t]*\\([^=:\n]+?\\)[ \t\n]*{[^{}]*?$" 1 'font-lock-type-face prepend)
	)
  "Keywords to highlight in Conf Bitbake mode.")

;;;###autoload
(defun bitbake-magic-match-p ()
  "Check if the current .inc file is a bitbake file.
Returns non-nil only if the buffer is visiting a file with a .inc
suffix that matches the regexp \"python[ \\t]+\\\\S +[ \\t]*()\", a
bitbake python function."
  (when (string-match "\\.inc\\'" (or (buffer-file-name) ""))
	(or (bitbake-conf-p)
		(save-excursion (re-search-forward "python[ \t]+\\S +[ \t]*()" nil t)))))

(defcustom bitbake-conf-file-name-regexps
  '("meta\\(-[^/]+\\)?/conf/.*\\.conf\\'"
	"meta/lib/.*/\\(layer\\)\\.conf\\'"
	"scripts/lib/.*/\\(layer\\|machine\\)\\.conf\\'")
  "List of regex for files that are assumed to be bitbake conf files.
Used by `bitbake-conf-p'."
  :group 'bitbake
  :type '(repeat regex)
  )

(defcustom bitbake-conf-file-name-exceptions
  '("/xorg\\.conf\\'")
  "List of regex for files that are assumed not to be bitbake conf files.
Used by `bitbake-conf-p'."
  :group 'bitbake
  :type '(repeat regex)
  )

(defun bitbake-conf-p ()
  "Analyze buffer content and to see if this is a bitbake file.
A three step process;

- first return false if any regex in
  `bitbake-conf-file-name-exceptions' matches the variable
  `buffer-file-name'.

- then return t if any of the regex in
  `bitbake-conf-file-name-regexps' matches the variable
  `buffer-file-name'.

- then return t if a any of set of internal regex's matches
  buffer content."
  (cl-labels ((buffer-name-match (re) (string-match re buffer-file-name)))
	(save-excursion
	  (goto-char (point-min))
	  (or (and (cl-notany #'buffer-name-match bitbake-conf-file-name-exceptions)
			   (cl-some #'buffer-name-match bitbake-conf-file-name-regexps))
		  (save-excursion (search-forward "bitbake" nil t))
		  ;; bitbake require statement
		  (save-excursion (re-search-forward "^\\s *require\\s *conf/.*\\.inc" nil t))
		  ;; Assignment with ?= with one or more ?
		  (save-excursion (re-search-forward "\\(.+?\\)\\(?:\\[\\(.*?\\)\\]\\)?[ \t]*\\?+=" nil t))
		  ;;inline python variable assignment
		  (save-excursion (re-search-forward "\\(.+?\\)\\(?:\\[\\(.*?\\)\\]\\)?[ \t]*\\?+=[ \t]*\"\\${@" nil t))
		  ;;add more options here as we discover them
		  ))))


;;;###autoload
(defun conf-bitbake-mode-maybe ()
  "Mode stub to redirect bitbake .conf files to `bitbake-mode'."
  (if (bitbake-conf-p)
	  (conf-bitbake-mode)
	(conf-mode)))

;;;###autoload
(define-derived-mode conf-bitbake-mode conf-mode "Conf[Bitbake]"  ()
  "Bitbake Conf Mode dialect.
Comments start with `#'.
For details see `conf-mode'.  Example:

# Conf mode font-locks this right on bitbake and with M-x conf-bitbake-mode

 [Desktop Entry]
	 Encoding=UTF-8
	 Name=The GIMP
	 Name[ca]=El GIMP
	 Name[cs]=GIMP"
  (conf-mode-initialize "#" 'conf-bitbake-font-lock-keywords)
  (setq-local comment-auto-fill-only-comments t)
  (auto-fill-mode)
  (bitbake-ff-setup))

(provide 'conf-bitbake-mode)
;; Local Variables:
;; tab-width: 4
;; End:
;;; conf-bitbake-mode.el ends here

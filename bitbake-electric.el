;;; bitbake-electric.el --- Electric actions for bitbake-mode

;; Copyright (C) 2014, 2018  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defvar bitbake-electric-layout-rules '((?= around " " :skip-before ":+."))
  "List of rules for bitbake electric actions.
Each rule has the form (CHAR ACTION [ARGS...]) where CHAR is the
char that was just inserted and ACTION specifies which action to
take.  ARGS can be any number of arguments to the ACTION.
ACTION can take the following values:
* a function: the function is called with point just after the
  inserted character and any ARGS passed as arguments.
* `before', or `after': The first ARG is inserted before or after
  CHAR, respectively.
* `around': The first ARG is inserted before CHAR, the second ARG
  after.  If there is only a single ARG, insert that before and
  after CHAR.
* nil: do nothing.")

(declare-function electric--after-char-pos "electric")
(defun bitbake-electric-actions ()
  (let ((rule (cdr (assq last-command-event bitbake-electric-layout-rules)))
		pos)
	(when (and rule
			   (setq pos (electric--after-char-pos)) ; FIXME: private electric function
			   ;; Not in a string ot comment - I hope! syntax-ppss seems to be lisp specific(?)
			   (not (nth 8 (save-excursion (syntax-ppss pos)))))
	  (let ((end (copy-marker (point) t))
			(action (car rule)))
		(goto-char pos)
		(pcase (if (functionp action) (apply action (cdr rule)) action)
		  (`before (let ((ins (nth 1 rule))
						 (skip-chars (nth 2 rule)))
					 (goto-char (1- pos))
					 (when skip-chars
					   (skip-chars-backward (if (stringp skip-chars)
												skip-chars
											  (char-to-string skip-chars))))
					 (insert ins)))
		  (`after (insert (nth 1 rule)))
		  (`around (let* ((before (nth 1 rule))
						  (after (if (nth 2 rule)
									 (if (keywordp (nth 2 rule))
										 before
									   (nth 2 rule))
								   before))
						  (skip-before (cadr (memq :skip-before rule))))
					 (save-excursion
					   (goto-char (1- pos))
					   (when skip-before
						 (skip-chars-backward (if (stringp skip-before)
												  skip-before
												(char-to-string skip-before))))
					   (insert (nth 1 rule)))
					 (insert after))))
		(goto-char end)))))

(define-minor-mode bitbake-electric-mode
  ""
  :group 'bitbake-electric
  (if bitbake-electric-mode
	  (add-hook 'post-self-insert-hook #'bitbake-electric-actions nil t)
	(remove-hook 'post-self-insert-hook #'bitbake-electric-actions t)))



(provide 'bitbake-electric)
;;; bitbake-electric.el ends here

[![CircleCI](https://circleci.com/bb/olanilsson/bitbake-modes.svg?style=shield&circle-token=ea3640f3d5d791f6d7a866474a3a8e0940824ceb)](https://circleci.com/bb/olanilsson/bitbake-modes)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

![emacs 28.1](https://img.shields.io/badge/emacs-28.1-brightgreen.svg)
![emacs 27.1](https://img.shields.io/badge/emacs-27.1-brightgreen.svg)
![emacs 26.3](https://img.shields.io/badge/emacs-26.3-brightgreen.svg)
![emacs 26.2](https://img.shields.io/badge/emacs-26.2-brightgreen.svg)
![emacs 26.1](https://img.shields.io/badge/emacs-26.1-brightgreen.svg)
![emacs 25.3](https://img.shields.io/badge/emacs-25.3-brightgreen.svg)
![emacs 25.2](https://img.shields.io/badge/emacs-25.2-brightgreen.svg)
![emacs 25.1](https://img.shields.io/badge/emacs-25.1-brightgreen.svg)
![emacs 24.4](https://img.shields.io/badge/emacs-24.4-brightgreen.svg)
![emacs 24.3](https://img.shields.io/badge/emacs-24.3-brightgreen.svg)

# bitbake-modes - Major modes for bitbake and related files

`bitbake-modes` is a collection of major modes and tools that aims to
be useful while working with the [bitbake][bitbakeproj] files in
the [Yocto][yocto] and [OpenEmbedded][oe] projects.

The code is in need of reorganization and sometimes rewriting, but it
has reached a stage where it is useful in daily work.

## Installation

`bitbake-modes` is not on [MELPA][melpa], but can be installed from
the authors [personal package archive][onpa].

```
   (add-to-list 'package-archives '("onpa" . "https://olanilsson.bitbucket.io/packages/"))
   (package-install "bitbake-modes")
```

## Contribute

Development happens on [bitbucket][bbm].

## Major modes

### conf-bitbake-mode

`conf-bitbake-mode` should be used for [bitbake configuration files][bbconf] -
files that contain no [bitbake functions][bbfunc].

### bitbake-mode

`bitbake-mode` can be used for any bitbake
file; [recipes][bbrecipes], [classes][bbclasses]
and [conf files][bbconf].  Patterns are added to `auto-mode-alist` for
`.bb`, `.bbappend`, and `.bbclass` files.

`bitbake-mode` uses `[mmm-mode][mmm]` to handle
[shell functions][shellfunc], [bitbake-style python functions][bbpyfunc]
and [python functions][pyfunc].  Inline python is not handled as an
mmm-region at this time.

### bb-scc-mode

`bb-scc-mode` is a major mode
for [Yocto][yocto] [kernel configuration scc files][scc].

### wks-mode

`wks-mode` is used for the `.wks` files used by `wic`,
the [OpenEmbedded][oe] [image generation][oepartimg] tool.

### bitbake-task-log-mode

`bitbake-task-log-mode` is a generic mode that is used for any
`log.do_*` file.  It has some `font-lock` keywords and functions to
easliy jump to the corresponding `run.do_*`file.

### bb-sh-mode

Not an actual mode, this function will force `shell-script-mode` into
the `sh` submode and adds a few extra font lock keywords for common
bitbake shell functions.

## Features

### mmm-mode

`bitbake-mode` have `mmm-classes` defined for shell functions and
both style of python functions.  The decoration levels and faces are
customizable.  Unfortunately indentation does not work very well in
the submodes at the moment.

### ff-find-other-file

Most of the included major modes have good `ff-find-other-file`
support. `bitbake-mode` recognizes `include`, `require`, `inherit`,
`BBCLASSEXTEND` statements and `file://` URIs.  For `inherit`,
`BBCLASSEXTEND` and URIs it will find the target closest to point.

Detecting layers to search is done by a customizable function, see the
variables `bitbake-ff-search-directories` and
`bitbake-topdir-dominating-file`.

### Visit the build dir

When visiting a recipe file, the `bitbake-recipe-build-dir-dired`
function will do its best to open the correct WORKDIR for the recipe.
If several build dirs are found, the user will be asked to select a
root directory.

[bbclasses]: http://www.yoctoproject.org/docs/2.7.1/bitbake-user-manual/bitbake-user-manual.html#recipes "Bitbake User Manual - Classes"
[bbconf]: http://www.yoctoproject.org/docs/2.7.1/bitbake-user-manual/bitbake-user-manual.html#configuration-files "Bitbake User Manual - Configuration Files"
[bbfunc]: http://www.yoctoproject.org/docs/2.7.1/bitbake-user-manual/bitbake-user-manual.html#functions "Bitbake User Manual - Functions"
[bbm]: https://bitbucket.org/olanilsson/bitbake-modes
[bbpyfunc]: http://www.yoctoproject.org/docs/2.7.1/bitbake-user-manual/bitbake-user-manual.html#bitbake-style-python-functions "Bitbake User Manual - Bitbake Style Python Functions"
[bbrecipes]: http://www.yoctoproject.org/docs/2.7.1/bitbake-user-manual/bitbake-user-manual.html#recipes "Bitbake User Manual - Recipes"
[bitbakeproj]: https://www.yoctoproject.org/tools-resources/projects/bitbake "bitbake project"
[melpa]: https://melpa.org "MELPA"
[mmm]: https://github.com/purcell/mmm-mode "mmm-mode"
[oe]: https://www.openembedded.org "OpenEmbedded"
[oepartimg]: http://www.yoctoproject.org/docs/2.7.1/dev-manual/dev-manual.html#creating-partitioned-images "Yocto Project Development Manual - Creating Partitioned Images"
[onpa]: https://olanilsson.bitbucket.io/packages
[pyfunc]: http://www.yoctoproject.org/docs/2.7.1/bitbake-user-manual/bitbake-user-manual.html#python-functions "Bitbake User Manual - Python Functions"
[scc]: http://www.yoctoproject.org/docs/2.7.1/kernel-dev/kernel-dev.html#scc-reference "Yocto Project Linux kernel Development Manual - SCC Description File Reference"
[shellfunc]: http://www.yoctoproject.org/docs/2.7.1/bitbake-user-manual/bitbake-user-manual.html#shell-functions "Bitbake User Manual - Shell Functions"
[yocto]: https://www.yoctoproject.org "Yocto Project"

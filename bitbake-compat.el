;;; bitbake-compat.el --- Compatibility functions for bitbake-modes  -*- lexical-binding: t; -*-

;; Copyright (C) 2017-2024  Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola x Nilsson <ola.x.nilsson@axis.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Backports of functions to support older Emacs versions.

;;; Code:

(when (and (version-list-< (version-to-list emacs-version) '(27 1))
		   (version-list-< '(24 3) (version-to-list emacs-version)))
  ;; Advice python-nav-beginning-of-statement to work around Emacs bug #30277
  (require 'python) ; make sure the function is defined
  (define-advice python-nav-beginning-of-statement
      (:around (oldfun &rest args) bitbake-workaround-emacs-bug-30277)
    "Early exit if point is on the first line of buffer or region.
Workaround for Emacs bug #30277."
    (forward-line 0)
    (if (not (bobp))
            (apply oldfun args)
      (back-to-indentation)
      (point-marker))))

(when (version-list-= (version-to-list emacs-version) '(25 1 1))
  (eval-when-compile (require 'rx))
  (eval-when-compile (require 'python))
  (declare-function python-info-assignment-statement-p "python")
  (declare-function python-info-looking-at-beginning-of-defun "python")
  (declare-function python-nav-backward-sexp "python")
  (declare-function python-nav-beginning-of-statement "python")
  (declare-function python-util-forward-comment "python")
  ;; python-info-docstring-p in Emacs 25.1 will infloop on bitbake
  ;; python functions (not defs) that has a docstring.  Backport this
  ;; version from Emacs 26.0.91.
  ;; TODO: Only use this function when fontifying mmm-regions in bitbake-mode?
  (defun bitbake-mmm-python-info-docstring-p (&optional syntax-ppss)
	"Return non-nil if point is in a docstring.
When optional argument SYNTAX-PPSS is given, use that instead of
point's current `syntax-ppss'."
  ;;; https://www.python.org/dev/peps/pep-0257/#what-is-a-docstring
	(save-excursion
      (when (and syntax-ppss (python-syntax-context 'string syntax-ppss))
		(goto-char (nth 8 syntax-ppss)))
      (python-nav-beginning-of-statement)
      (let ((counter 1)
			(indentation (current-indentation))
			(backward-sexp-point)
			(re (concat "[uU]?[rR]?"
						(python-rx string-delimiter))))
		(when (and
               (not (python-info-assignment-statement-p))
               (looking-at-p re)
               ;; Allow up to two consecutive docstrings only.
               (>=
				2
				(let (last-backward-sexp-point)
                  (while (save-excursion
                           (python-nav-backward-sexp)
                           (setq backward-sexp-point (point))
                           (and (= indentation (current-indentation))
								;; Make sure we're always moving point.
								;; If we get stuck in the same position
								;; on consecutive loop iterations,
								;; bail out.
								(prog1 (not (eql last-backward-sexp-point
												 backward-sexp-point))
                                  (setq last-backward-sexp-point
										backward-sexp-point))
								(looking-at-p
								 (concat "[uU]?[rR]?"
										 (python-rx string-delimiter)))))
					;; Previous sexp was a string, restore point.
					(goto-char backward-sexp-point)
					(cl-incf counter))
                  counter)))
          (python-util-forward-comment -1)
          (python-nav-beginning-of-statement)
          (cond ((bobp))
				((python-info-assignment-statement-p) t)
				((python-info-looking-at-beginning-of-defun))
				(t nil))))))

  (defvar mmm-primary-mode)
  (declare-function bitbake-mmm-python-info-docstring-p "bitbake-compat" (&optional syntax-ppss))
  (define-advice python-info-docstring-p
      (:around (oldfun &rest args) bitbake-workaround-python-docstring-infloop)
    "Avoid infloop in `python-info-docstring-p'."
	(if (and (boundp 'mmm-mode)
			 mmm-mode
			 (eq mmm-primary-mode 'bitbake-mode))
        (apply #'bitbake-mmm-python-info-docstring-p args)
      (apply oldfun args)))
  )

(provide 'bitbake-compat)
;;; bitbake-compat.el ends here

;;; bitbake-insert.el --- Various insert functions for bitbake-mode -*- lexical-binding: t; -*-

;; Copyright (C) 2014-2024  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords: languages, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Bitbake supports the following assignement operators:
;;  =   normal expand-later assignment
;;  ?=  weak expand-later assignment
;;  ??= even weaker expand-later assignment
;;  :=  expand-now assigment
;;  +=  append with a separating space character
;;  =+  prepend with a separating space character
;;  .=  append without a separating space character
;;  =.  prepend without a separating space character

;; Using C-M-i for 'VAR = "|"' is reasonable.  What should the
;; prefixes do?  C-u C-M-i for 'VAR ?= "|"' and C-u C-u C-M-i for 'VAR
;; ??= "|"' is reasonable, but the ?= and ??= are not used that much.
;; += would be much more useful.
;; What about a multiple-choice question?


;;; Code:

(defun bitbake-insert-var (variable-name &optional assign-type)
  "Prepare definition of a new variable VARIABLE-NAME.
If ASSIGN-TYPE is set it should be a valid bitbake assignment type."
  (interactive "sVariable Name: ")
  (unless (zerop (length variable-name))
	(beginning-of-line)
	(insert variable-name " " (or assign-type "=") " \"\"")
	(backward-char)))

(defun bitbake-append-var (variable-name)
  "Prepare a `+=' append to VARIABLE-NAME."
  (interactive "sVariable Name: ")
  (bitbake-insert-var variable-name "+="))

(defun bitbake-insert-override (variable override)
  "Prepare an VARIABLE OVERRIDE assignment."
  (interactive "Mvariable: \nMoverride: ")
  (insert variable ":" override " = "))

(provide 'bitbake-insert)
;;; bitbake-insert.el ends here

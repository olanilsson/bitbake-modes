# Copyright (C) 2014-2019 by Ola Nilsson <ola.nilsson@gmail.com>

EMACS ?= emacs
ifeq ($(EMACS),t)
EMACS = emacs
endif

BATCH = --batch -q -l .emacs/lisp/init.el
EMACS_BATCH = $(EMACS) $(BATCH)
EMACS_PACKAGING = $(EMACS) --batch -q -l .emacs/lisp/packaging.el
TESTFILEFLAGS = $(addprefix -l ,$(wildcard test/*.el))
EMACS_TEST = $(EMACS) $(BATCH) $(TESTFILEFLAGS)

# The JUnit report file, different for each ci-service
circleci_junit = $(if $(CIRCLECI),$(CIRCLE_WORKING_DIRECTORY)/test_results/ert/test.xml)
shippable_junit = $(if $(SHIPPABLE),shippable/testresults/tests.xml)
pipelines_junit = $(if $(BITBUCKET_BUILD_NUMBER),test-results/junit.xml)
JUNIT ?= $(or $(circleci_junit),$(shippable_junit),$(pipelines_junit),junit.xml)
$(if $(word 2,$(JUNIT)),$(error JUNIT may only contain one filename, was "$(JUNIT)"))

all: build

setup:
	$(EMACS_BATCH)

build:
	$(EMACS_BATCH) --eval '(byte-recompile-directory "'$(PWD)'" 0)'

# run tests w/o junit report
check ert:
	$(EMACS_TEST) -f ert-run-tests-batch-and-exit

# run tests with junit report
report test: $(JUNIT)

# Ensure that JUNIT is always regenerated
.PHONY: $(JUNIT)
$(JUNIT):
	mkdir -p $(dir $(JUNIT))
	$(EMACS_TEST) -l ert-junit -f ert-junit-run-tests-batch-and-exit $(JUNIT)

## Packaging

bitbake-modes-pkg.el: bitbake-modes.el
	$(EMACS_PACKAGING) bitbake-modes.el -f generate-description-file

dist/bitbake-modes-readme.txt: bitbake-modes.el
	mkdir -p $(dir $@)
	$(EMACS_PACKAGING) bitbake-modes.el -f generate-readme > $@

package_name := $(shell $(EMACS_PACKAGING) bitbake-modes.el -f package-archive-name 2>/dev/null)
package_dir = $(basename $(package_name))

tar_has_exclude_ignore := $(shell tar --help 2>&1 | sed -n '/exclude-ignore=/s/.*/yes/p')
ifeq ($(tar_has_exclude_ignore),yes)

package_contents = $(filter-out %/,$(shell tar -cvf /dev/null --exclude dist --exclude-ignore=.elpaignore --exclude-vcs --exclude-backup . 2>&1))

contents:
	@echo $(package_contents) | tr ' ' '\n'

package: dist/$(package_name) dist/bitbake-modes-readme.txt

dist/$(package_name): bitbake-modes-pkg.el $(package_contents)
	mkdir -p $(dir $@)
	tar -cvf $@ --transform='s/\./'$(package_dir)'/' --exclude dist --exclude-ignore=.elpaignore --exclude-vcs --exclude-backup .
else
package contents dist/$(package_name):
	@echo "The $@ target requires tar to support the --exclude-ignore option."
	@echo "This version of tar does not"
	@tar --version
	@false
endif

## circleci CLI commands

ifneq (,$(filter cicheck%,$(MAKECMDGOALS)))
# The circleci tool will fail if two commands are run at the same
# time, so disable parallelism if any cicheck is a command line
# target.
.NOTPARALLEL:
endif

cichecks = $(patsubst %,cicheck_%.cilog,24.3 24.4 24.5 25.1 25.2 25.3 26.1)
cicheck: $(cichecks)
	@echo && echo CI Summary
	@for log in $^ ; do \
	  grep --binary-file=text 'Success!' -H $$log || \
        echo $$log:'Failure!' ;\
	done

# Ensure that cichecks always are rerun
.PHONY: $(cichecks)
$(cichecks):
	rm -rf *.elc test/*.elc
	circleci build --job=$(patsubst cicheck_%.cilog,emacs_%,$@) | tee $@
	grep --binary-file=text -q 'Success!' $@

lisp-clean:
	-rm -rf *.elc test/*.elc

clean: lisp-clean
	rm -rf dist *.cilog

reallyclean: clean
	rm -rf .emacs/elpa .emacs/.emacs-custom.el

.PHONY: all test clean lisp-clean reallyclean cicheck

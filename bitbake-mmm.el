;;; bitbake-mmm.el --- `mmm-mode' setup for `bitbake-mode' -*- lexical-binding: t; -*-

;; Copyright (C) 2014-2024  Ola Nilsson

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Declarations:


;;; Code:

(require 'mmm-mode)
(require 'bitbake-compat)
(require 'sh-script)

(defun bitbake--mmm-def-back-offset ()
  "Move point backwards across empty lines.
Used as `:back-offset' function for the `bitbake-python-def'
`mmm-class'.  Point should typically be at the beginning of a
non-blank line.  Move point back to the beginning of the first
preceeding non-blank line, then forward to the beginning of the
next line, which should be blank."
  (forward-line -1)
  (while (looking-at-p "^\\s-*$")
	(forward-line -1))
  (forward-line))

(mmm-add-group 'bitbake-function
  `(;;python functions starting with `def'
	;; Contains all lines up to but not including the next line
    ;; starting with non-whitespace, or the end of buffer.  Use a
	;; :back-offset function to not include trailing blank lines in
	;; the region.
	(bitbake-python-def
	 :submode python-mode
	 :face bitbake-python-function-face
     :front ,(rx bol "def" (+ blank))
	 :include-front t
     :back ,(rx bol (or (not (any space "\n")) buffer-end))
     :back-offset (bitbake--mmm-def-back-offset)
     :insert ((?d bitbake-python-def "Bitbake Python def: "
                  @ @ "def " str "():\n" "    " _  "\n\n" @ @))
	 )
	;;python functions starting with `python'
	(bitbake-python-python
	 :submode python-mode
	 :face bitbake-python-task-face
     :front ,(rx bol (? "fakeroot" (* blank))
                 "python" (* blank)
                 (? (+ (not blank)) (* blank))
                 "(" (* blank) ")"
                 (* blank)
                 "{" (* blank) "\n")
	 :include-front nil
	 :back "^}"
	 :include-back nil
     :insert ((?p bitbake-python-function "Bitbake Python Function: "
                  @ "python " str "() {\n" @ "    " _ @ "\n}" @)
              (?o bitbake-fakeroot-python-function "Bitbake Fakeroot Python Function: "
                  @ "fakeroot python " str "() {\n" @ "    " _ @ "\n}" @)
              (?a bitbake-anon-python-function ?p . "")
              )
	 )
	;;inline python
	;; (bitbake-python-inline
	;;  :submode python-mode
	;;  :face bitbake-python-expansion-face
	;;  :front "\\${@"
	;;  :include-front nil
	;;  :back "}"
	;;  :include-back nil)
	;;shell functions
	(bitbake-shell-function
	 :submode bb-sh-mode
	 :face bitbake-shell-function-face
     :front ,(rx bol (? "fakeroot" (+ blank))
                 (group-n 1
                   (group-n 2 (+ (any word "-_.+{}$:")))
                   (* blank) "()" (* blank) "{" ))
     :front-verify (lambda () (not (string= "python" (match-string 2))))
	 :front-match 1
	 :include-front t
	 :front-offset 0
	 :back "^}"
	 :include-back t
     :insert ((?s bitbake-shell-function "Bitbake Shell function: "
                  @ @ str "() {\n\t" _ "\n}" @ @)
              (?w bitbake-fakeroot-shell-function "Bitbake Fakeroot Shell function: "
                  @ @ "fakeroot " str "() {\n\t" _ "\n}" @ @))
     )
     ))

(defconst bitbake-mmm-extra-sh-var-list-default
  '(comint-dynamic-complete-functions
	comint-prompt-regexp
	comment-start
	comment-start-skip
	defun-prompt-regexp
	font-lock-defaults
	imenu-generic-expression
	indent-line-function
	paragraph-separate
	paragraph-start
	parse-sexp-ignore-comments
	parse-sexp-lookup-properties
	require-final-newline
	sh-basic-offset
	sh-header-marker
	sh-here-doc-markers
	sh-here-doc-re
	sh-indent-supported-here
	sh-indentation
	sh-kw-alist
	sh-mode-syntax-table
	sh-regexp-for-done
	sh-shell
	sh-shell-file
	sh-shell-variables
	sh-shell-variables-initialized
	skeleton-end-hook
	skeleton-filter-function
	skeleton-newline-indent-rigidly
	skeleton-pair-alist
	skeleton-pair-default-alist
	skeleton-pair-filter-function)
  "Default value for `bitbake-extra-sh-var-alist'.")

(defcustom bitbake-mmm-extra-sh-var-list bitbake-mmm-extra-sh-var-list-default
  "A list of variable symbols added to `mmm-save-local-variables'.
All symbols in `sh-var-list' are also added.  Note that symbols in
this list only are added at load time, so hooks have to add
variables directly to `mmm-save-local-variables'."
  :options bitbake-mmm-extra-sh-var-list-default
  :group 'bitbake
  :type '(repeat variable))

(defun bitbake-mmm-local-vars-setup ()
  "Make `mmm-mode' keep more `shell-script-mode' variables.
All variables listed in `bitbake-mmm-extra-sh-var-list' and
`sh-var-list' will be added to `mmm-save-local-variables'."
  (require 'sh-script)
  (mapc (lambda (varname)
		  (add-to-list 'mmm-save-local-variables (list varname 'buffer)))
		(append bitbake-mmm-extra-sh-var-list sh-var-list))
  (add-to-list 'mmm-save-local-variables (list 'indent-tabs-mode 'buffer)))

(eval-after-load "mmm-vars" '(bitbake-mmm-local-vars-setup))

(defcustom bitbake-mmm-decoration-level 2
  "Amount of coloring to use in bitbake submode regions.
See `mmm-submode-decoration-level'."
  :group 'bitbake
  :type '(choice (const :tag "None" 0)
                 (const :tag "Low" 1)
                 (const :tag "High" 2)))

(defun bitbake-mmm-reparse ()
  "Call `mmm-parse-buffer' if variable `mmm-mode' is set in the current buffer."
  (when mmm-mode
	(save-excursion (mmm-parse-buffer))))

;;;###autoload
(defun bitbake-mmm-setup ()
  "Set up function `mmm-mode' for `bitbake-mode'."
  (set (make-local-variable 'mmm-submode-decoration-level)
       bitbake-mmm-decoration-level)
  (setq mmm-classes 'bitbake-function)
  (mmm-mode-on)
  ;; FIXME: add mmm-reparse in more hooks
  (add-hook 'after-save-hook #'bitbake-mmm-reparse nil t)
  (add-hook 'after-revert-hook #'bitbake-mmm-reparse nil t)
  (add-hook 'ediff-prepare-buffer-hook 'mmm-mode-off t t)
  (add-hook 'ediff-after-quit-hook-internal
            (lambda() (mmm-mode-on) (mmm-parse-buffer)) t t))

(provide 'bitbake-mmm)
;;; bitbake-mmm.el ends here

;;; scc.el --- Modes for Yocto Kernel configuration fragments -*- lexical-binding: t; -*-

;; Copyright (C) 2014-2024  Ola Nilsson <ola.nilsson@gmail.com>

;; Author: Ola Nilsson <ola.nilsson@gmail.com>
;; Keywords: languages, tools,

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'find-file)
(require 'bitbake-compat)

(defgroup bitbake-kconf nil
  ""
  :group 'bitbake)

(defun bb-scc-mode-font-lock-keywords ()
  "Return the default font lock keywords for `bb-scc-mode'."
   `(("^[ \t]*\\(define\\)[ \t]+\\([^ \t]+\\)[ \t]+\".*\""
	  (1 'font-lock-keyword-face)
	  (2 'font-lock-variable-name-face))
	 ("^[ \t]*\\(branch\\)" 1 'font-lock-keyword-face)
	 ("^[ \t]*\\(define\\)[ \t]+\\([^ \t]+\\)"
	  (1 'font-lock-keyword-face)
	  (2 'font-lock-variable-name-face))
	 ("^[ \t]*\\(include\\)[[:blank:]]+\\([^[:blank:]]+\\.scc\\)\\(\\(?:[[:blank:]]+nocfg\\)?\\)"
	  (1 'font-lock-preprocessor-face)
	  (2 'font-lock-string-face)
	  (3 'font-lock-type-face))
	 ("^[ \t]*\\(kconf\\)[[:blank:]]+\\(\\(?:non-\\)?hardware\\)"
	  (1 'font-lock-keyword-face)
	  (2 'font-lock-type-face))
	 ("^[ \t]*\\(git\\)[[:blank:]]+\\(merge\\)[[:blank:]]+\\([^[:blank:]]+\\)"
	  (1 'font-lock-keyword-face)
	  (2 'font-lock-keyword-face))
	 ("^[ \t]*\\(patch\\)" . 'font-lock-keyword-face)
	 ))

;;;###autoload
(define-derived-mode bb-scc-mode prog-mode "SCC"
  "Major mode for bitbake kernel configuration SCC files.
\\{bb-scc-mode-map}"
  :group 'bitbake-kconf
  (setq-local font-lock-defaults '(bb-scc-mode-font-lock-keywords))
  (setq-local ff-ignore-include t)
  (setq-local ff-special-constructs
			  `(("^[ \t]*include[ \t]+\\(.*\\)" . ,(apply-partially 'match-string-no-properties 1))
				("^[ \t]*kconf[ \t]+\\(?:non-\\)?hardware[ \t]+\\(.*\\)" . ,(apply-partially 'match-string-no-properties 1))))
  )

(modify-syntax-entry ?\# "<" bb-scc-mode-syntax-table)
(modify-syntax-entry ?\n ">" bb-scc-mode-syntax-table)

(provide 'scc)
;;; scc.el ends here
